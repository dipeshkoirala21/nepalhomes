/* all routing setup go here */
import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import { createBottomTabNavigator } from 'react-navigation-tabs';

import Ionicons from 'react-native-vector-icons/Ionicons';
//import LastScreen from './screens/welcome';

import WalkThrough from './screens/walkthrough';

// import DrawerNavigator from './screens/home/components/DrawerNavigator';

import NewsScreen from './screens/news';
import NewsDetail from './screens/newsdetail';
import MenuScreen from './screens/menu';
import ProfileScreen from '../src/screens/profile';
import ProfileInformation from '../src/screens/profileinfo';
import ForgotPassword from '../src/screens/forgotpassword';
import SearchScreen from '../src/screens/searches';
import DetailScreen from '../src/screens/detail';
import Terms from '../src/screens/termsandconditions';
import TypeDetails from '../src/screens/typedetail';
import Login from '../src/screens/login';
import Signup from '../src/screens/signup';
import Userprofile from './screens/loggedinprofile';
import ChangePassword from '../src/screens/changepassword';
import AddProperty from '../src/screens/addproperty';
import AddProperty2 from '../src/screens/addproperty2';
import AddProperty3 from '../src/screens/addproperty3';
import EMICalculator from '../src/screens/emicalculator';
import UnitConverter from '../src/screens/unitconverter/index';
import HomeScreen from '../src/screens/home/index';

const WelcomeNavigator = createStackNavigator(
  {
    WalkThrough,
  },
  {
    defaultNavigationOptions: () => ({
      header: null,
    }),
  },
);

const Explore = createStackNavigator(
  {
    HomeScreen: {
      screen: HomeScreen,

      navigationOptions: () => {
        return {
          header: null,
          // tabBarIcon: ({ tintColor }) => (
          //   <Icon name="home" size={30} color="#900" />
          // ),
          // headerLeft: (
          //   <Icon
          //     style={{ paddingLeft: 10 }}
          //     onPress={() => navigation.openDrawer()}
          //     name="md-menu"
          //     size={30}
          //   />
          // )
        };
      },
    },
  },
  {
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
  },
);

const News = createStackNavigator({
  NewsScreen: {
    screen: NewsScreen,
    navigationOptions: () => {
      return {
        header: null,
      };
    },
  },
});

const Menu = createStackNavigator({
  MenuScreen: {
    screen: MenuScreen,
    navigationOptions: () => {
      return {
        header: null,
      };
    },
  },
});
const PasswordChange = createStackNavigator({
  ChangePassword: {
    screen: ChangePassword,
    navigationOptions: () => {
      return {
        headerTitle: 'Change Password',
      };
    },
  },
});
const PasswordForgot = createStackNavigator({
  ForgotPassword: {
    screen: ForgotPassword,
    navigationOptions: () => {
      return {
        headerTitle: 'Forgot Password',
      };
    },
  },
});
// static navigationOptions = {
//   headerTitle: 'Details',
//   headerStyle: {
//     elevation: 0,
//     marginTop: -24,
//     backgroundColor: '#371a5c',
//     height: 56,
//   },
//   headerTintColor: '#fff',
//   headerTitleStyle: {
//     fontWeight: 'bold',
//     fontSize: 16,
//   },
// };
const Profile = createSwitchNavigator({
  ProfileScreen: {
    screen: ProfileScreen,
    navigationOptions: () => {
      return {
        header: null,
      };
    },
  },
  Login: {
    screen: Login,
    navigationOptions: () => {
      return {
        header: null,
      };
    },
  },
  Signup: {
    screen: Signup,
    navigationOptions: () => {
      return {
        header: null,
      };
    },
  },
  Userprofile: {
    screen: Userprofile,
    navigationOptions: () => {
      return {
        header: null,
      };
    },
  },
  PasswordChange,
  PasswordForgot,
});

const DashboardTabNavigator = createBottomTabNavigator(
  {
    Explore,
    News,
    Profile,
    Menu,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'Explore') {
          iconName = 'ios-search';
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          // IconComponent = HomeIconWithBadge;
        } else if (routeName === 'News') {
          iconName = 'ios-paper';
        } else if (routeName === 'Profile') {
          iconName = 'md-person';
        } else if (routeName === 'Menu') {
          iconName = 'md-menu';
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: '#75C3ED',
      style: {
        backgroundColor: '#202B8B',
        height: 60,
      },
    },
  },
);

const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: {
      screen: DashboardTabNavigator,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    SearchScreen: {
      screen: SearchScreen,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    DetailScreen: {
      screen: DetailScreen,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    NewsDetail: {
      screen: NewsDetail,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    AddProperty: {
      screen: AddProperty,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    AddProperty2: {
      screen: TypeDetails,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    AddProperty3: {
      screen: AddProperty3,
      navigationOptions: () => {
        return {
          headerTitle: null,
        };
      },
    },
    ProfileInformation: {
      screen: ProfileInformation,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    Terms: {
      screen: Terms,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    EMICalculator: {
      screen: EMICalculator,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    UnitConverter: {
      screen: UnitConverter,
      navigationOptions: () => {
        return {
          header: null,
        };
      },
    },
    TypeDetails: {
      screen: TypeDetails,
      navigationOptions: () => {
        return {
          headerTitle: 'Details',
        };
      },
    },
  },
  // {
  //   defaultNavigationOptions: () => {
  //     return {
  //       header: null,
  //     };
  //   },
  // },
);

export const AppWelcomeContainer = createAppContainer(WelcomeNavigator);
export const AppContainer = createAppContainer(DashboardStackNavigator);
