/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image,
  Picker,
} from 'react-native';

import {
  formatAmount,
  trimDecimal,
  MONTH,
  MONTH_OPTIONS,
  YEAR_OPTIONS,
} from './utils';

class EMICalculator extends Component {
  state = {
    principal: '',
    rate: '',
    years: '',
    emi: '',
    totalInterest: '',
    totalPayable: '',
    isYearly: true,
    loanMonthlyBreakdown: [],
    startDate: new Date(),
  };
  handleChange = name => text => this.setState({ [name]: text });
  calculateLoan = () => {
    const { principal, rate, years, isYearly, startDate } = this.state;

    const P = +principal;
    const Rate = +rate;
    const Years = +years;

    if (P > 0 && Rate > 0 && Years > 0) {
      const R = Rate / 1200;
      const N = isYearly ? Years * 12 : Years;
      const EMI = (P * R * (1 + R) ** N) / ((1 + R) ** N - 1);
      const totalPayableValue = EMI * N;
      const totalInterestValue = totalPayableValue - P;
      this.setState({ emi: trimDecimal(EMI) });
      this.setState({ totalPayable: trimDecimal(totalPayableValue) });
      this.setState({ totalInterest: trimDecimal(totalInterestValue) });
      let mi = '';
      let map = '';
      let lP = P;
      const tempDate = new Date(startDate);
      const loanMonthlyBreakdownValue = Array(N)
        .fill(null)
        .map(() => {
          const e = tempDate.getMonth() + 1;
          mi = lP * R;
          map = EMI - mi;
          lP -= map;
          const result = {
            month: MONTH[tempDate.getMonth()],
            year: tempDate.getFullYear(),
            principal: map,
            interest: mi,
            emi: EMI,
            balance: lP,
          };
          tempDate.setMonth(e);
          return result;
        });
      this.setState({
        loanMonthlyBreakdown: loanMonthlyBreakdownValue,
      });
    }
    this.setState({
      principal: '',
      rate: '',
      years: '',
    });
  };
  render() {
    // console.log(this.state);
    const { loanMonthlyBreakdown } = this.state;
    return (
      <KeyboardAvoidingView
        style={{ flex: 1, bottom: 10 }}
        behavior="padding"
        enabled
      >
        <ScrollView>
          <View
            style={{
              marginHorizontal: 20,
              marginTop: 40,
            }}
          >
            <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Menu')}
              >
                <Image
                  style={{ height: 25, width: 25, marginTop: 20 }}
                  source={require('../../../assets/left_blue.png')}
                />
              </TouchableOpacity>
              <Text
                style={{
                  marginTop: 20,
                  marginLeft: 20,
                  color: '#3F51B5',
                  fontWeight: 'bold',
                  fontSize: 15,
                }}
              >
                EMI Calculator
              </Text>
            </View>
            <View>
              <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 20 }}>
                Loan Amount
              </Text>
              <TextInput
                style={{
                  height: 50,
                  width: '100%',
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  borderRadius: 5,
                  marginTop: 10,
                  padding: 10,
                }}
                value={this.state.principal}
                onChangeText={this.handleChange('principal')}
                placeholder={'(Rs.)xxxxxxxxx'}
              />
            </View>
            <View>
              <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 20 }}>
                Term(Years)
              </Text>
              <TextInput
                style={{
                  height: 50,
                  width: '100%',
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  borderRadius: 5,
                  marginTop: 10,
                  padding: 10,
                }}
                value={this.state.years}
                onChangeText={this.handleChange('years')}
                placeholder={'Years'}
              />
            </View>
            <View>
              <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 20 }}>
                Interest(%)
              </Text>
              <TextInput
                style={{
                  height: 50,
                  width: '100%',
                  borderWidth: 1,
                  borderColor: '#d3d3d3',
                  borderRadius: 5,
                  marginTop: 10,
                  padding: 10,
                }}
                value={this.state.rate}
                onChangeText={this.handleChange('rate')}
                placeholder={'Interest'}
              />
            </View>
            <View>
              <Text style={{ fontSize: 16, fontWeight: 'bold', marginTop: 20 }}>
                Starting From
              </Text>
              <View
                style={{
                  marginTop: 10,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <View
                  style={{
                    borderRadius: 5,
                    borderColor: '#979797',
                    borderWidth: 1,
                  }}
                >
                  <Picker
                    selectedValue={`${this.state.startDate.getMonth()}`}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) => {
                      const newMonth = +itemValue;
                      const newDate = new Date(
                        this.state.startDate.getFullYear(),
                        newMonth,
                        1,
                      );
                      this.setState({ startDate: newDate });
                    }}
                  >
                    {MONTH_OPTIONS.map((each, index) => (
                      <Picker.Item
                        label={each.text}
                        value={each.value}
                        key={each.text}
                      />
                    ))}
                  </Picker>
                </View>
                <View
                  style={{
                    borderRadius: 5,
                    borderColor: '#979797',
                    borderWidth: 1,
                  }}
                >
                  <Picker
                    selectedValue={`${this.state.startDate.getFullYear()}`}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) => {
                      const newYear = +itemValue;
                      const newDate = new Date(
                        newYear,
                        this.state.startDate.getMonth(),
                        1,
                      );
                      this.setState({ startDate: newDate });
                    }}
                  >
                    {YEAR_OPTIONS.map((each, index) => (
                      <Picker.Item
                        label={each.text}
                        value={each.value}
                        key={each.text}
                      />
                    ))}
                  </Picker>
                </View>
              </View>
            </View>
            <TouchableOpacity
              style={{
                marginTop: 20,
                width: '100%',
                height: 40,
                backgroundColor: '#202B8B',
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#006395',
                borderWidth: 1,
                borderRadius: 4,
              }}
              onPress={() => this.calculateLoan()}
            >
              <Text
                style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
              >
                Calculate
              </Text>
            </TouchableOpacity>
            {/* {console.log(this.state)} */}
            {this.state.emi ? (
              <View
                style={{
                  marginTop: 20,
                }}
              >
                <View
                  style={{
                    marginTop: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Text style={{ fontWeight: 'bold', fontSize: 16 }}>
                    Monthly Payment
                  </Text>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 24,
                      color: '#202B8B',
                    }}
                  >
                    {formatAmount(this.state.emi)}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                >
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 16,
                        marginTop: 20,
                      }}
                    >
                      Total Interest
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#202B8B',
                      }}
                    >
                      {formatAmount(this.state.totalInterest)}
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 16,
                        marginTop: 20,
                      }}
                    >
                      Payable Amount
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#202B8B',
                      }}
                    >
                      {formatAmount(this.state.totalPayable)}
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 20,
                    height: 50,
                    width: '100%',
                    backgroundColor: '#202B8B',
                    alignItems: 'center',
                    justifyContent: 'space-around',
                    flexDirection: 'row',
                  }}
                >
                  <Text
                    style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
                  >
                    Month-Year
                  </Text>
                  <Text
                    style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
                  >
                    Pricipal(A)
                  </Text>
                  <Text
                    style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
                  >
                    Interest(B)
                  </Text>
                </View>
                {loanMonthlyBreakdown.map(each => (
                  <View
                    key={`${each.month}-${each.year}`}
                    style={{
                      marginTop: 10,
                      width: '100%',
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      flexDirection: 'row',
                      borderBottomWidth: 1,
                      borderBottomColor: '#d3d3d3',
                    }}
                  >
                    <Text
                      style={{
                        color: '#000',
                        fontSize: 15,
                        fontWeight: 'normal',
                      }}
                    >
                      {each.month}-{each.year}
                    </Text>
                    <Text
                      style={{
                        color: '#000',
                        fontSize: 15,
                        fontWeight: 'normal',
                      }}
                    >
                      {formatAmount(trimDecimal(each.principal))}
                    </Text>
                    <Text
                      style={{
                        color: '#000',
                        fontSize: 15,
                        fontWeight: 'normal',
                      }}
                    >
                      {formatAmount(trimDecimal(each.interest))}
                    </Text>
                  </View>
                ))}
              </View>
            ) : null
              // <Text>loading</Text>
            }
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

export default EMICalculator;
