/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  Modal,
  FlatList,
  Picker,
  ToastAndroid,
} from 'react-native';

import { setIsFirstLoad } from '../../redux/app/app.actions';

import {
  selectRecentData,
  selectDataLoading,
} from '../../redux/property/property.selectors';
import { recentpropertyData } from '../../redux/property/property.actions';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import FilterModal from '../component/modal';
import tempImg3 from '../../../assets/home.png';
import { IMAGE_URL } from '../../api';

import HotProperty from '../component/hotproperty';
import TrendingProperty from '../component/trendingproperty';
import ProjectsProperty from '../component/projectproperty';
import MyLoader from '../component/propertyskeleton';

export class Homescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searches: [],
      modalVisible: false,
    };
  }
  setModalVisible = visible => {
    this.setState({ modalVisible: visible });
  };
  async componentDidMount() {
    try {
      const item = await this.props.recentpropertyData();
      this.setState({ searches: item.data.data });
    } catch (error) {
      console.log(error);
    }
  }
  formatToNepaliStyle(amt) {
    if (!amt) {
      return '0';
    }
    const amtStr = `${amt}`;
    const indexOfPeriod = amtStr.indexOf('.');
    let sliceIndex = indexOfPeriod - 1;
    if (indexOfPeriod === -1) {
      sliceIndex = amtStr.length - 1;
    }
    const part1 = amtStr.slice(0, sliceIndex);
    const part2 = amtStr.slice(sliceIndex);
    const withCommas = part1.toString().replace(/\B(?=(\d{2})+(?!\d))/g, ',');
    return `${withCommas}${part2}`;
  }
  formatAmount = value => {
    return `Rs. ${this.formatToNepaliStyle(value)}`;
  };
  renderRecentItem = ({ item }) => {
    const { recentdata } = this.props;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#FFFFFF',
          flexDirection: 'column',
          marginHorizontal: 10,
          borderRadius: 5,
          borderColor: '#fff',
          elevation: 4,
          borderWidth: 1,
          alignItems: 'center',
          position: 'relative',
          marginBottom: 20,
          width: 300,
        }}
      >
        {Object.keys(recentdata).length > 0 && (
          <TouchableOpacity
            activeOpacity={1}
            onPress={() =>
              this.props.navigation.navigate('TypeDetails', {
                slug: item.slug_url,
              })
            }
          >
            <Image
              style={{
                height: 280,
                width: 300,
                borderTopWidth: 0.5,
                borderColor: '#fff',
                borderTopLeftRadius: 5,
                borderTopRightRadius: 5,
              }}
              source={
                item.media && item.media.images.length > 0
                  ? {
                    uri: `${IMAGE_URL}${item.media.images[0].id.path}`,
                  }
                  : tempImg3
              }
            />
            <View>
              <View style={{ width: '100%' }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 18,
                    color: '#000',
                    marginTop: 5,
                    marginLeft: 10,
                  }}
                  numberOfLines={1}
                >
                  {item.basic.title}
                </Text>
                <Text
                  style={{
                    fontWeight: 'normal',
                    fontSize: 15,
                    color: '#8A8A8F',
                    marginTop: 5,
                    marginLeft: 10,
                  }}
                >
                  {item.address.area_id && item.address.area_id.name
                    ? item.address.area_id.name
                    : 'loading'}
                  {', '}
                  {item.address.city_id && item.address.city_id.name
                    ? item.address.city_id.name
                    : 'loading'}
                </Text>
                <Text
                  style={{
                    fontWeight: 'normal',
                    fontSize: 18,
                    color: '#202B8B',
                    marginTop: 5,
                    marginLeft: 10,
                  }}
                >
                  {item.price && item.price.value
                    ? this.formatAmount(item.price.value)
                    : 'loading'}
                </Text>
              </View>
              {/* <View
                style={{
                  width: '20%',
                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                  marginTop: 20,
                }}
              >
                <View
                  style={{
                    width: 0,
                    height: 0,
                    borderLeftWidth: 20,
                    borderRightWidth: 0,
                    borderBottomWidth: 25,
                    borderStyle: 'solid',
                    backgroundColor: 'transparent',
                    borderLeftColor: '#fff',
                    borderRightColor: '#fff',
                    borderBottomColor: '#D3D3D3',
                    bottom: 0,
                    marginTop: 5,
                  }}
                />
                <View
                  style={{
                    height: 30,
                    width: 50,
                    borderTopLeftRadius: 8,
                    backgroundColor: '#D3D3D3',
                    justifyContent: 'center',
                    alignItems: 'center',
                    bottom: 0,
                    borderBottomRightRadius: 5,
                  }}
                >
                  <Text style={{ color: '#202B8B', fontWeight: 'bold' }}>
                    20 Cr
                  </Text>
                </View>
              </View> */}
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  };
  render() {
    const { loading } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <View
          style={{
            borderWidth: 0.5,
            borderColor: '#202B8B',
            borderRadius: 20,
            flex: 1,
            backgroundColor: '#F4F4F7',
            bottom: 10,
          }}
        >
          <ScrollView>
            <View style={styles.shadowView}>
              <View style={styles.searchSection}>
                <Image
                  style={{
                    height: 30,
                    width: 30,
                    marginTop: 12,
                    marginLeft: 10,
                  }}
                  source={require('../../../assets/search.png')}
                />

                <TouchableOpacity
                  onPress={() => this.setState({ modalVisible: true })}
                  style={{
                    width: '90%',
                    height: 40,
                    marginTop: 5,
                    backgroundColor: '#fff',
                    marginHorizontal: 5,
                    paddingHorizontal: 20,
                    borderRadius: 4,
                    borderWidth: 0.3,
                    borderColor: '#fff',
                    color: '#000',
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                  }}
                >
                  <Text style={{ color: '#4A4A4A' }}>
                    SEARCH HOMES, APARTMENTS...
                  </Text>
                </TouchableOpacity>
              </View>
              <FilterModal
                modalVisible={this.state.modalVisible}
                setModalVisible={this.setModalVisible}
                navigate={this.props.navigation.navigate}
              />
            </View>

            <View style={styles.banner}>
              <Image
                style={{ height: 50, width: 60, marginTop: 10 }}
                source={require('../../../assets/free.png')}
              />
              <View>
                <Text
                  style={{ fontSize: 16, fontWeight: 'bold', color: '#73B103' }}
                >
                  0% Commission Guaranteed
                </Text>
                <Text style={{ fontSize: 12, fontWeight: 'normal' }}>
                  NepalHomes.com ensure you best deals{'\n'}with no commision.
                </Text>
              </View>
            </View>
            <View
              style={{
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={{ color: '#202B8B', fontSize: 20 }}>RECENT </Text>
              <Text style={{ color: '#202B8B', fontSize: 20 }}>PROJECTS</Text>
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View
                style={{ justifyContent: 'space-around', flexDirection: 'row' }}
              >
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
              </View>
            </ScrollView>
            <ProjectsProperty navigate={this.props.navigation.navigate} />
            <View
              style={{
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={{ color: 'red', fontSize: 20 }}>PREMIUM </Text>
              <Text style={{ color: '#202B8B', fontSize: 20 }}>PROPERTIES</Text>
            </View>
            <View
              style={{
                marginTop: 2,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={{ fontSize: 13 }}>
                exclusive showcase of properties
              </Text>
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View
                style={{ justifyContent: 'space-around', flexDirection: 'row' }}
              >
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
              </View>
            </ScrollView>
            <HotProperty navigate={this.props.navigation.navigate} />
            <View
              style={{
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={{ color: '#0291DD', fontSize: 20 }}>TRENDING </Text>
              <Text style={{ color: '#202B8B', fontSize: 20 }}>PROPERTIES</Text>
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View
                style={{ justifyContent: 'space-around', flexDirection: 'row' }}
              >
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
              </View>
            </ScrollView>
            <TrendingProperty navigate={this.props.navigation.navigate} />
            <View
              style={{
                marginTop: 30,
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={{ color: '#FF9500', fontSize: 20 }}>RECENT </Text>
              <Text style={{ color: '#202B8B', fontSize: 20 }}>PROPERTIES</Text>
            </View>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View
                style={{ justifyContent: 'space-around', flexDirection: 'row' }}
              >
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
                {loading && <MyLoader />}
              </View>
            </ScrollView>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={this.state.searches}
              renderItem={this.renderRecentItem}
              keyExtractor={item => item._id}
            />
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#202B8B',
  },
  shadowView: {
    height: 50,
    backgroundColor: 'white',
    borderRadius: 5,
    marginHorizontal: 10,
    marginTop: 80,
    shadowOpacity: 3,
    shadowRadius: 4,
    elevation: 3,
  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginHorizontal: 10,
  },

  banner: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    height: 80,
    backgroundColor: '#EDFCEF',
    borderRadius: 5,
    marginHorizontal: 10,
    marginTop: 30,
    elevation: 3,
  },
});

const mapStateToProps = createStructuredSelector({
  recentdata: selectRecentData,
  loading: selectDataLoading,
});
const mapDispatchToProps = {
  recentpropertyData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Homescreen);
