/* eslint-disable prettier/prettier */
/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  Image,
  TextInput,
  Modal,
  Dimensions,
} from 'react-native';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { profileInfo } from '../../redux/auth/auth.actions';
import { changeprofileInfo } from '../../redux/auth/auth.actions';
import { setProfileData } from '../../redux/auth/auth.actions';
import { profilePhotoUpload } from '../../redux/auth/auth.actions';
import { selectProfileInfo } from '../../redux/auth/auth.selectors';
import { selectToken } from '../../redux/app/app.selectors';
import { selectData } from '../../redux/auth/auth.selectors';
import { IMAGE_URL } from '../../api';
import tempImg3 from '../../../assets/home.png';
class ProfileInfo extends Component {
  state = {
    modalVisible: false,
    // name: this.props.data.name,
    // email: this.props.data.email,
    image: null,
    showCamera: false,
  };

  handleGallery = async () => {
    // const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    // console.log(permission, 'camera roll permission');
    // if (permission.status === 'granted') {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result, 'hello');

    if (result.cancelled) {
      //not granted
      console.log('not granted');
      alert('Sorry, we need camera roll permissions to make this work!');
      return;
    }
    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
    const splits = result.uri.split('/');
    const name = splits[splits.length - 1];
    this.props.profilePhotoUpload({
      uri: result.uri,
      name,
      type: name.substr(name.length - 3) === 'png' ? 'image/png' : 'image/jpeg',
    });
    this.setModalVisible(false);
  };
  handleCamera = async () => {
    const permission = await Permissions.getAsync(
      Permissions.CAMERA,
      Permissions.CAMERA_ROLL,
    );
    console.log(permission, 'camera roll permission');
    if (permission.status === 'granted') {
      let result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });

      console.log(result, 'hello');

      if (!result.cancelled) {
        this.setState({ image: result.uri });
      }
      const splits = result.uri.split('/');
      const name = splits[splits.length - 1];
      this.props.profilePhotoUpload({
        uri: result.uri,
        name,
        type:
          name.substr(name.length - 3) === 'png' ? 'image/png' : 'image/jpeg',
      });
      this.setModalVisible(false);
    } else if (permission.status !== 'granted') {
      const newPermission = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.CAMERA_ROLL,
      );
      if (newPermission.status === 'granted') {
        //its granted.
        console.log('grantted');
        let result = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });

        if (!result.cancelled) {
          this.setState({ image: result.uri });
        }
        const splits = result.uri.split('/');
        const name = splits[splits.length - 1];
        this.props.profilePhotoUpload({
          uri: result.uri,
          name,
          type:
            name.substr(name.length - 3) === 'png' ? 'image/png' : 'image/jpeg',
        });
        this.setModalVisible(false);
      }
    } else {
      //not granted
      console.log('not granted');
      alert('Sorry, we need camera roll permissions to make this work!');
    }
  };
  componentDidMount() {
    this.props.profileInfo();
  }
  handleChange = name => text =>
    this.props.setProfileData({ key: name, value: text });

  setModalVisible = visible => {
    this.setState({ modalVisible: visible });
  };

  _handleBack = () => {
    this.props.navigation.navigate('Userprofile');
  };

  handleSave = () => {
    const { name, email, image } = this.props.data;
    // const splits = this.state.image.split('/');
    // const imageName = splits[splits.length - 1];
    this.props.changeprofileInfo({
      name,
      email,
      image,
    });
  };
  //  console.log(this.state.name, this.state.email, this.state.image);
  render() {
    //const { token } = this.props;
    //  console.log(this.props.data);
    //let { image } = this.state;
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ marginHorizontal: 20, marginTop: 40 }}>
          {/* {this.state.showCamera && (
            <CameraPage />
          )} */}
          <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>
            <TouchableOpacity onPress={this._handleBack}>
              <Image
                style={{ height: 25, width: 25, marginTop: 20 }}
                source={require('../../../assets/left_blue.png')}
              />
            </TouchableOpacity>
            <Text
              style={{
                marginTop: 20,
                marginLeft: 20,
                color: '#3F51B5',
                fontWeight: 'bold',
                fontSize: 15,
              }}
            >
              PROFILE
            </Text>
          </View>

          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 30,
            }}
          >
            <Image
              style={{
                height: 70,
                width: 70,
                borderWidth: 1,
                borderColor: '#fff',
                borderRadius: 35,
              }}
              source={
                this.props.data && this.props.data.image && Object.keys(this.props.data.image).length > 0 && this.props.data.image.path.length > 0
                  ? {
                    uri: `${IMAGE_URL}${this.props.data.image.path}`,
                  }
                  : tempImg3
              }
            />
            <TouchableOpacity onPress={() => this.setModalVisible(true)}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#0291DD',
                }}
              >
                change picture
              </Text>
            </TouchableOpacity>
          </View>
          <TextInput
            style={{
              height: 50,
              width: '100%',
              borderWidth: 1,
              borderColor: '#979797',
              borderRadius: 5,
              padding: 10,
              marginTop: 20,
            }}
            value={this.props.data.name}
            onChangeText={this.handleChange('name')}
            placeholder={'Name'}
          />
          <TextInput
            style={{
              height: 50,
              width: '100%',
              borderWidth: 1,
              borderColor: '#979797',
              borderRadius: 5,
              padding: 10,
              marginTop: 10,
            }}
            value={this.props.data.email}
            onChangeText={this.handleChange('email')}
            placeholder={'Email'}
          />
          <View>
            <Modal
              style={{ justifyContent: 'center' }}
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                this.setModalVisible(false);
              }}
            >
              <View
                style={{
                  height: Dimensions.get('window').height,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.setModalVisible(false)}
                  style={{ flex: 1 }}
                />
                <View
                  style={{
                    backgroundColor: '#fff',
                    height: 170,
                    marginTop: 20,
                    marginHorizontal: 10,
                    borderWidth: 1,
                    borderColor: '#fff',
                    borderRadius: 5,
                    elevation: 8,
                  }}
                >
                  <TouchableOpacity
                    style={{
                      width: '100%',
                      height: 50,
                      backgroundColor: '#0291DD',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderColor: '#0291DD',
                      borderWidth: 1,
                      borderRadius: 4,
                    }}
                    onPress={this.handleGallery}
                  >
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 15,
                        fontWeight: 'bold',
                      }}
                    >
                      Upload From Gallery
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '100%',
                      height: 50,
                      backgroundColor: '#0291DD',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderColor: '#0291DD',
                      borderWidth: 1,
                      borderRadius: 4,
                    }}
                    onPress={this.handleCamera}
                  // onPress={() => <CameraPage />}
                  //  onPress={() => <Apple />}
                  >
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 15,
                        fontWeight: 'bold',
                      }}
                    >
                      Take Picture
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      width: '100%',
                      height: 50,
                      marginTop: 10,
                      backgroundColor: '#fff',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderColor: '#C8C7CC',
                      borderWidth: 1,
                      borderRadius: 4,
                    }}
                    onPress={() => this.setModalVisible(false)}
                  >
                    <Text
                      style={{
                        color: '#FF3B30',
                        fontSize: 15,
                        fontWeight: 'bold',
                      }}
                    >
                      Cancel
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
            <TouchableOpacity
              style={{
                marginTop: 20,
                width: '100%',
                height: 40,
                backgroundColor: '#202B8B',
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#006395',
                borderWidth: 1,
                borderRadius: 4,
              }}
              onPress={this.handleSave}
            >
              <Text
                style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
              >
                SAVE
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: selectData,
  token: selectToken,
  selectProfileInfo,
});
const mapDispatchToProps = {
  profileInfo,
  changeprofileInfo,
  profilePhotoUpload,
  setProfileData,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileInfo);
