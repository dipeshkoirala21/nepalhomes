/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, Image, ScrollView, SafeAreaView } from 'react-native';
import { WebView } from 'react-native-webview';
import { TouchableOpacity } from 'react-native-gesture-handler';

export class index extends Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <WebView
          source={{
            uri: `https://nh.wafttech.com/news/mobile/${this.props.navigation.getParam(
              'slug',
            )}`,
          }}
          originWhitelist={['*']}
          textZoom={100}
          containerStyle={{
            marginTop: 10,
            marginHorizontal: 10,
          }}
        />
        {/* <View>
          <Image
            style={{
              height: 300,
              width: '100%',
              backgroundColor: 'white',
              borderRadius: 1,
              borderWidth: 0.5,
              borderColor: '#fff',
              shadowOpacity: 3,
              marginBottom: 10,
              position: 'relative',
            }}
            source={this.props.navigation.getParam('image')}
          />
          <View
            style={{
              position: 'absolute',
              marginTop: 40,
              width: '100%',
              top: 0,
              left: 0,
              paddingHorizontal: 16,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('News')}
                style={{ height: 30, width: 30, left: 0 }}
              >
                <Image
                  style={{
                    height: 20,
                    width: 20,
                  }}
                  source={require('../../../assets/left_white.png')}
                />
              </TouchableOpacity>

              <TouchableOpacity style={{ height: 30, width: 30, right: 0 }}>
                <Image
                  style={{
                    height: 20,
                    width: 20,
                  }}
                  source={require('../../../assets/share.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View>
          <Text
            style={{
              color: '#000',
              marginLeft: 20,
              fontSize: 12,
              fontWeight: 'normal',
              marginTop: 10,
            }}
          >
            Jun 19th 2019
          </Text>
          <Text
            style={{
              color: '#000',
              marginLeft: 20,
              fontSize: 18,
              fontWeight: 'bold',
            }}
          >
            {this.props.navigation.getParam('title')}
          </Text>
        </View>
        <WebView
          source={{ html: this.props.navigation.getParam('desc') }}
          originWhitelist={['*']}
          textZoom={300}
          containerStyle={{
            marginTop: 20,
            marginHorizontal: 20,
          }}
        />
      </> */}
      </SafeAreaView>
    );
  }
}

export default index;
