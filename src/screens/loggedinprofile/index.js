/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { selectToken } from '../../redux/app/app.selectors';
import { selectData } from '../../redux/auth/auth.selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { logout } from '../../redux/auth/auth.actions';
import { profileInfo } from '../../redux/auth/auth.actions';
import { IMAGE_URL } from '../../api';
import tempImg3 from '../../../assets/home.png';
import { clearPostPropertyField } from '../../redux/property/property.actions';

class LoggedIn extends Component {
  state = {};
  _handlerOnPress = () => {
    this.props.logout();
  };
  componentDidMount() {
    this.props.profileInfo();

    // console.log(this.props.data);
    //console.log(this.props.data.image.path);
  }

  static getDerivedStateFromProps = nextProps => {
    if (!nextProps.token) {
      console.log('here loggedinprofile', nextProps.token);
      nextProps.navigation.navigate('ProfileScreen');
    }
    return null;
  };
  handleTerms = () => {
    this.props.navigation.navigate('Terms');
  };
  addProperty = () => {
    this.props.navigation.navigate('AddProperty');
    this.props.clearPostPropertyField();
  }

  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#202B8B',
        }}
      >
        <View
          style={{
            borderWidth: 0.5,
            borderColor: '#202B8B',
            borderRadius: 20,
            flex: 1,
            backgroundColor: '#F4F4F7',
            bottom: 10,
          }}
        >
          <ScrollView>
            <View
              style={{
                flex: 1,
                marginHorizontal: 10,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                  height: 80,
                  backgroundColor: '#fff',
                  borderRadius: 5,
                  marginHorizontal: 10,
                  marginTop: 80,
                  elevation: 4,
                }}
              >
                <Image
                  style={{
                    height: 70,
                    width: 70,
                    marginLeft: 10,
                    borderWidth: 1,
                    borderColor: '#fff',
                    borderRadius: 35,
                  }}
                  source={
                    this.props.data && this.props.data.image.path
                      ? {
                        uri: `${IMAGE_URL}${this.props.data.image.path}`,
                      }
                      : tempImg3
                  }
                />
                <View style={{ marginLeft: 20 }}>
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: 'bold',
                      color: '#202B8B',
                    }}
                  >
                    {this.props.data && this.props.data.name ? this.props.data.name : <Text style={{
                      fontSize: 18,
                      fontWeight: 'bold',
                      color: '#202B8B',
                    }}> Nepal Homes</Text>}
                  </Text>

                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: 'normal',
                      color: '#979797',
                      marginTop: 5,
                    }}
                  >
                    member since oct 20
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate('ProfileInformation')
                  }
                >
                  <Image
                    style={{ height: 30, width: 30, marginLeft: 40 }}
                    source={require('../../../assets/edit.png')}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  height: 120,
                  backgroundColor: '#fff',
                  borderRadius: 5,
                  marginHorizontal: 10,
                  marginTop: 20,
                  elevation: 4,
                }}
              >
                <View
                  style={{
                    justifyContent: 'space-between',
                    marginTop: 20,
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'normal',
                      color: '#979797',
                    }}
                  >
                    You can Post for free and we don't {'\n'}charge any types of
                    commisions
                  </Text>
                  <TouchableOpacity
                    onPress={() => this.addProperty()}
                    style={{ marginTop: 10 }}
                  >
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: 'normal',
                        color: '#0291DD',
                      }}
                    >
                      ADD PROPERTY
                    </Text>
                  </TouchableOpacity>
                </View>
                <Image
                  style={{ height: 60, width: 60 }}
                  source={require('../../../assets/profilehome.png')}
                />
              </View>
              <View style={{ marginHorizontal: 10, marginTop: 30 }}>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#1C3D5A',
                  }}
                >
                  LEGAL
                </Text>
                <TouchableOpacity
                  onPress={this.handleTerms}
                  style={{
                    marginTop: 20,
                    borderBottomWidth: 1,
                    borderBottomColor: '#D8D8D8',
                    borderStyle: 'dashed',
                  }}
                >
                  <Text
                    style={{
                      color: '#1C3D5A',
                      fontSize: 17,
                      fontWeight: 'normal',
                    }}
                  >
                    Terms and Conditions
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    marginTop: 20,
                    borderBottomWidth: 1,
                    borderBottomColor: '#D8D8D8',
                    borderStyle: 'dashed',
                  }}
                >
                  <Text
                    style={{
                      color: '#1C3D5A',
                      fontSize: 17,
                      fontWeight: 'normal',
                    }}
                  >
                    Privacy Policy
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginHorizontal: 10, marginTop: 30 }}>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    color: '#1C3D5A',
                  }}
                >
                  SECURITY
                </Text>
                <TouchableOpacity
                  style={{
                    marginTop: 20,
                    borderBottomWidth: 1,
                    borderBottomColor: '#D8D8D8',
                    borderStyle: 'dashed',
                  }}
                  onPress={() =>
                    this.props.navigation.navigate('ChangePassword')
                  }
                >
                  <Text
                    style={{
                      color: '#1C3D5A',
                      fontSize: 17,
                      fontWeight: 'normal',
                    }}
                  >
                    Change Password
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    marginTop: 20,
                    borderBottomWidth: 1,
                    borderBottomColor: '#D8D8D8',
                    borderStyle: 'dashed',
                  }}
                  onPress={() => this._handlerOnPress()}
                >
                  <Text
                    style={{
                      color: '#1C3D5A',
                      fontSize: 17,
                      fontWeight: 'normal',
                    }}
                  >
                    Logout
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  token: selectToken,
  data: selectData,
});

const mapDispatchToProps = { logout, profileInfo, clearPostPropertyField };

export default connect(mapStateToProps, mapDispatchToProps)(LoggedIn);
