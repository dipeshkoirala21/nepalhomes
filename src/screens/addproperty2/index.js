/* eslint-disable prettier/prettier */
/* eslint-disable radix */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  Button,
  TouchableOpacity,
  Image,
  Picker,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import { enumsData } from '../../redux/enums/enums.actions';
import { selectEnumsData } from '../../redux/enums/enums.selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { setPostPropertyValuePrice } from '../../redux/property/property.actions';
import { selectPropertyDataPrice } from '../../redux/property/property.selectors';
import { setPostPropertyValueAddress } from '../../redux/property/property.actions';
import { selectPropertyDataAddress } from '../../redux/property/property.selectors';
import { selectLocationData } from '../../redux/location/location.selectors';
import { locationData } from '../../redux/location/location.actions';
import {
  setPostPropertyValueBuildingNoOf,
  setPostPropertyValueBuilding,
} from '../../redux/property/property.actions';
import { selectPropertyDataBuilding } from '../../redux/property/property.selectors';

class AddProperty2 extends Component {
  async componentDidMount() {
    try {
      await this.props.enumsData();
      await this.props.locationData();
      // console.log(await selectLocationData);
    } catch (error) {
      //do sth
      console.log(error);
    }
  }
  // handleChange = name => text =>
  //   this.props.setPostPropertyValuePrice({ key: name, value: text });

  onPriceValueSelected = (id, key) => {
    this.props.setPostPropertyValuePrice({
      key,
      value: id,
    });
  };
  onAddressValueSelected = (id, key) => {
    this.props.setPostPropertyValueAddress({
      key,
      value: id,
    });
  };

  onNoOfRoomSelected = (key, value) => {
    this.props.setPostPropertyValueBuildingNoOf({
      // key: 'property_purpose',
      key,
      value,
    });
  };
  onBuildingSelected = (key, value) => {
    this.props.setPostPropertyValueBuilding({
      // key: 'property_purpose',
      key,
      value,
    });
  };
  render() {
    const {
      data,
      selectEnumsData,
      selectLocationData,
      selectPropertyDataAddress,
      selectPropertyDataBuilding,
    } = this.props;
    const ROOM = [1, 2, 3, 4, 5, 6];
    // console.log(selectLocationData.allState, 'all state data');
    return (
      <KeyboardAvoidingView style={{ flex: 1, bottom: 10 }} behavior="padding" enabled>
        <ScrollView>
          <View style={{ marginHorizontal: 20, flex: 1, marginBottom: 20 }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 50,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('AddProperty')}
                >
                  <Image
                    style={{ height: 25, width: 25 }}
                    source={require('../../../assets/left_blue.png')}
                  />
                </TouchableOpacity>
                <View style={{ flexDirection: 'column' }}>
                  <Text
                    style={{
                      marginLeft: 20,
                      color: '#3F51B5',
                      fontWeight: 'normal',
                      fontSize: 18,
                    }}
                  >
                    Post Property
                </Text>
                  <Text
                    style={{ color: '#666666', fontSize: 10, marginLeft: 20 }}
                  >
                    Step 2 of 3
                </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 50,
                }}
              >
                <Button
                  title="Next"
                  onPress={() => this.props.navigation.navigate('AddProperty3')}
                  style={{ width: 40, marginTop: 50, height: 20 }}
                />
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
                height: 80,
                backgroundColor: '#fff',
                borderRadius: 5,
                marginTop: 30,
                elevation: 4,
              }}
            >
              <View style={{ width: '85%', marginLeft: 10 }}>
                <Text
                  style={{ fontSize: 14, fontWeight: 'normal', color: '#4A4A4A' }}
                >
                  If you have any difficulty in filling the form,{'\n'}Call us at
                  166000-99999 (Toll Free)
              </Text>
              </View>
              <View
                style={{
                  backgroundColor: '#4CD964',
                  width: '15%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderBottomLeftRadius: 40,
                  borderTopLeftRadius: 40,
                  borderStartWidth: 1,
                  borderBottomWidth: 1,
                  borderColor: '#4CD964',
                  borderTopRightRadius: 5,
                  borderBottomEndRadius: 4,
                }}
              >
                <Image
                  style={{ height: 30, width: 30 }}
                  source={require('../../../assets/phone.png')}
                />
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                PRICE
            </Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                }}
              >
                <Picker
                  selectedValue={data.currency}
                  style={{
                    width: 100,
                    height: 50,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onPriceValueSelected(itemValue, 'currency')
                  }
                >
                  {selectEnumsData.currency ? (
                    selectEnumsData.currency.map(each => (
                      <Picker.Item
                        label={each.title}
                        value={each._id}
                        key={each._id}
                      />
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </Picker>
              </View>
              <TextInput
                style={{
                  height: 52,
                  borderWidth: 1,
                  borderColor: '#979797',
                  borderRadius: 5,
                  padding: 10,
                }}
                value={data.value}
                onChangeText={text => this.onPriceValueSelected(text, 'value')}
                placeholder={'Property Price'}
              />
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                }}
              >
                <Picker
                  selectedValue={data.label}
                  style={{
                    width: 130,
                    height: 50,
                  }}
                  name="label"
                  onValueChange={(itemValue, itemIndex) =>
                    this.onPriceValueSelected(itemValue, 'label')
                  }
                >
                  {selectEnumsData.price_label ? (
                    selectEnumsData.price_label.map(each => (
                      <Picker.Item
                        label={each.title}
                        value={each._id}
                        key={each._id}
                      />
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </Picker>
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                ADDRESS
            </Text>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                  width: 150,
                }}
              >
                {Object.keys(selectLocationData).length > 0 && (
                  <Picker
                    selectedValue={selectPropertyDataAddress.state_id}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      this.onAddressValueSelected(itemValue, 'state_id')
                    }
                  >
                    {selectLocationData.allState ? (
                      selectLocationData.allState.map(each => (
                        <Picker.Item
                          label={each.name}
                          value={each._id}
                          key={each._id}
                        />
                      ))
                    ) : (
                        <Text>loading...</Text>
                      )}
                  </Picker>
                )}
              </View>
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                  width: 150,
                }}
              >
                {Object.keys(selectLocationData).length > 0 && (
                  <Picker
                    selectedValue={selectPropertyDataAddress.district_id}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      this.onAddressValueSelected(itemValue, 'district_id')
                    }
                  >
                    {selectLocationData.allDistrict ? (
                      selectLocationData.allDistrict.map(each => (
                        <Picker.Item
                          label={each.name}
                          value={each._id}
                          key={each._id}
                        />
                      ))
                    ) : (
                        <Text>loading...</Text>
                      )}
                  </Picker>
                )}
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
              }}
            >
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                  width: 150,
                }}
              >
                {Object.keys(selectLocationData).length > 0 && (
                  <Picker
                    selectedValue={selectPropertyDataAddress.city_id}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      this.onAddressValueSelected(itemValue, 'city_id')
                    }
                  >
                    {selectLocationData.allVdc ? (
                      selectLocationData.allVdc.map(each => (
                        <Picker.Item
                          label={each.name}
                          value={each._id}
                          key={each._id}
                        />
                      ))
                    ) : (
                        <Text>loading...</Text>
                      )}
                  </Picker>
                )}
              </View>
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                  width: 150,
                }}
              >
                {Object.keys(selectLocationData).length > 0 && (
                  <Picker
                    selectedValue={selectPropertyDataAddress.area_id}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      this.onAddressValueSelected(itemValue, 'area_id')
                    }
                  >
                    {selectLocationData.allArea ? (
                      selectLocationData.allArea.map(each => (
                        <Picker.Item
                          label={each.name}
                          value={each._id}
                          key={each._id}
                        />
                      ))
                    ) : (
                        <Text>loading...</Text>
                      )}
                  </Picker>
                )}
              </View>
            </View>
            <TextInput
              style={{
                height: 52,
                width: '100%',
                borderWidth: 1,
                borderColor: '#979797',
                borderRadius: 5,
                padding: 10,
                marginTop: 10,
              }}
              value={selectPropertyDataAddress.house_no}
              onChangeText={text => this.onAddressValueSelected(text, 'house_no')}
              placeholder={'House No., Near Baneshowr Chowk'}
            />
            <Text
              style={{
                fontSize: 10,
                fontWeight: 'normal',
                color: '#1C3D5A',
                marginTop: 5,
                marginLeft: 10,
              }}
            >
              Above address will not be visible to others.
          </Text>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                BEDROOM
            </Text>
            </View>
            <ScrollView
              horizontal
              style={{ flex: 1 }}
              showsHorizontalScrollIndicator={false}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 20,
                  bottom: 10,
                  marginHorizontal: 10,
                }}
              >
                {ROOM.map(each => (
                  <TouchableOpacity
                    key={each}
                    onPress={() => this.onNoOfRoomSelected('bedroom', each)}
                    style={{
                      height: 50,
                      width: 50,
                      backgroundColor: '#fff',
                      borderColor:
                        selectPropertyDataBuilding.bedroom === each
                          ? '#0291DD'
                          : '#202B8B',
                      borderWidth: 1,
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 20,
                    }}
                  >
                    <Text style={{ color: '#202B8B' }}>{each}</Text>
                  </TouchableOpacity>
                ))}
                <TextInput
                  style={{
                    height: 50,
                    width: 150,
                    borderWidth: 1,
                    borderColor: '#979797',
                    borderRadius: 5,
                    padding: 10,
                  }}
                  value={selectPropertyDataBuilding.bedroom}
                  onChangeText={text =>
                    this.onNoOfRoomSelected('bedroom', parseInt(text))
                  }
                  placeholder={'No. of Bedrooms'}
                />
              </View>
            </ScrollView>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                KITCHEN
            </Text>
            </View>
            <ScrollView
              horizontal
              style={{ flex: 1 }}
              showsHorizontalScrollIndicator={false}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 20,
                  bottom: 10,
                  marginHorizontal: 10,
                }}
              >
                {ROOM.map(each => (
                  <TouchableOpacity
                    key={each}
                    onPress={() => this.onNoOfRoomSelected('kitchen', each)}
                    style={{
                      height: 50,
                      width: 50,
                      backgroundColor: '#fff',
                      borderColor:
                        selectPropertyDataBuilding.kitchen === each
                          ? '#0291DD'
                          : '#202B8B',
                      borderWidth: 1,
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 20,
                    }}
                  >
                    <Text style={{ color: '#202B8B' }}>{each}</Text>
                  </TouchableOpacity>
                ))}
                <TextInput
                  style={{
                    height: 50,
                    width: 150,
                    borderWidth: 1,
                    borderColor: '#979797',
                    borderRadius: 5,
                    padding: 10,
                  }}
                  value={selectPropertyDataBuilding.kitchen}
                  onChangeText={text =>
                    this.onNoOfRoomSelected('kitchen', parseInt(text))
                  }
                  placeholder={'No. of Kitchens'}
                />
              </View>
            </ScrollView>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                BATHROOM
            </Text>
            </View>
            <ScrollView
              horizontal
              style={{ flex: 1 }}
              showsHorizontalScrollIndicator={false}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 20,
                  bottom: 10,
                  marginHorizontal: 10,
                }}
              >
                {ROOM.map(each => (
                  <TouchableOpacity
                    key={each}
                    onPress={() => this.onNoOfRoomSelected('bathroom', each)}
                    style={{
                      height: 50,
                      width: 50,
                      backgroundColor: '#fff',
                      borderColor:
                        selectPropertyDataBuilding.bathroom === each
                          ? '#0291DD'
                          : '#202B8B',
                      borderWidth: 1,
                      borderRadius: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 20,
                    }}
                  >
                    <Text style={{ color: '#202B8B' }}>{each}</Text>
                  </TouchableOpacity>
                ))}
                <TextInput
                  style={{
                    height: 50,
                    width: 150,
                    borderWidth: 1,
                    borderColor: '#979797',
                    borderRadius: 5,
                    padding: 10,
                  }}
                  value={selectPropertyDataBuilding.bathroom}
                  onChangeText={text =>
                    this.onNoOfRoomSelected('bathroom', parseInt(text))
                  }
                  placeholder={'No. of Bathrooms'}
                />
              </View>
            </ScrollView>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                PARKING
            </Text>
            </View>
            <TextInput
              style={{
                height: 50,
                width: '100%',
                borderWidth: 1,
                borderColor: '#979797',
                borderRadius: 5,
                padding: 10,
              }}
              value={selectPropertyDataBuilding.parking}
              onChangeText={text => this.onBuildingSelected('parking', text)}
              placeholder={'Parking Space'}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  data: selectPropertyDataPrice,
  selectEnumsData,
  selectLocationData,
  selectPropertyDataAddress,
  selectPropertyDataBuilding,
});
const mapDispatchToProps = {
  enumsData,
  setPostPropertyValuePrice,
  locationData,
  setPostPropertyValueAddress,
  setPostPropertyValueBuilding,
  setPostPropertyValueBuildingNoOf,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddProperty2);
