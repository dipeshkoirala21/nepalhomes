/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';

import {
  selectData,
  selectLoading,
  selectBlog,
  selectBlogCategory,
} from '../../redux/news/news.selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import {
  newsData,
  blogData,
  blogCategoryData,
} from '../../redux/news/news.actions';
import SafeAreaView from 'react-native-safe-area-view';
import { IMAGE_URL } from '../../api';
import MyLoader from '../component/skeleton';

import tempImg3 from '../../../assets/home.png';
class NewsScreen extends Component {
  state = {
    news: [],
    selected: 'all',
  };

  async componentDidMount() {
    try {
      const item = await this.props.newsData();
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState({ news: item.data.data });
      await this.props.blogCategoryData();
    } catch (error) {
      //do sth
      console.log(error);
    }
  }

  selectedOptions = slug_url => {
    this.props.blogData(slug_url);
    this.setState({ selected: slug_url });
    if (this.state.selected === '') {
      this.props.newsData();
    }
  };
  // _selected = () => { };
  renderItem = ({ item }) => (
    <View
      style={{
        height: 400,
        width: '100%',
        backgroundColor: '#fff',
        borderRadius: 5,
        borderColor: '#000',
        marginTop: 20,
        elevation: 4,
        bottom: 10,
      }}
    >
      <TouchableOpacity
        activeOpacity={1}
        onPress={() =>
          this.props.navigation.navigate('NewsDetail', {
            // image:
            //   item.image && item.image.path.length > 0
            //     ? {
            //       uri: `${IMAGE_URL}${item.image.path}`,
            //     }
            //     : tempImg3,
            // title: item.title,
            // desc: item.description,
            slug: item.slug_url,
          })
        }
      >
        <Image
          style={{
            height: 300,
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 5,
            shadowOpacity: 3,
            shadowRadius: 4,
            marginBottom: 10,
          }}
          //source={{ uri: `${IMAGE_URL}${item.image.path}` }}
          source={
            item.image && item.image.path.length > 0
              ? {
                uri: `${IMAGE_URL}${item.image.path}`,
              }
              : tempImg3
          }
        />
        <Text
          style={{
            color: '#000',
            marginLeft: 10,
            fontSize: 18,
            fontWeight: 'bold',
          }}
        >
          {item.title}
        </Text>
        <Text
          style={{
            color: '#000',
            marginLeft: 10,
            fontSize: 12,
            fontWeight: 'normal',
            marginTop: 10,
          }}
        >
          Jun 19th 2019
        </Text>
      </TouchableOpacity>
    </View>
  );
  render() {
    //console.log(this.props.data.data[0].category[0].title);
    //console.log(this.props.data);
    const { data, loading, blogtype } = this.props;
    //  console.log(blogtype);
    // console.log(this.state.selected);
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#202B8B',
        }}
      >
        <View
          style={{
            borderWidth: 0.5,
            borderColor: '#202B8B',
            borderRadius: 20,
            flex: 1,
            backgroundColor: '#F4F4F7',
            bottom: 10,
          }}
        >
          <ScrollView>
            <View style={{ marginHorizontal: 10, marginTop: 60 }}>
              <Text
                style={{
                  color: '#202B8B',
                  fontSize: 26,
                  fontWeight: 'bold',
                }}
              >
                News
              </Text>
              <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.selectedOptions('all')}
                    style={{
                      width: 100,
                      height: 40,
                      backgroundColor: '#fff',
                      borderColor:
                        this.state.selected === 'all' ? '#0291DD' : '#4A4A4A',
                      borderWidth: 1,
                      borderRadius: 20,
                      marginTop: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 10,
                      marginBottom: 10,
                    }}
                  >
                    <Text> All </Text>
                  </TouchableOpacity>
                  {blogtype.data ? (
                    blogtype.data.map(each => (
                      <TouchableOpacity
                        key={each._id}
                        onPress={() => this.selectedOptions(each.slug_url)}
                        style={{
                          width: 100,
                          height: 40,
                          backgroundColor: '#fff',
                          borderColor:
                            each.slug_url === this.state.selected
                              ? '#0291DD'
                              : '#4A4A4A',
                          borderWidth: 1,
                          borderRadius: 20,
                          marginTop: 20,
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginRight: 10,
                          marginBottom: 10,
                        }}
                      >
                        <Text>{each.title}</Text>
                      </TouchableOpacity>
                    ))
                  ) : (
                      <Text />
                    )}
                </View>
              </ScrollView>
              {loading && <MyLoader />}
              {loading && <MyLoader />}
              {loading && <MyLoader />}
              {loading && <MyLoader />}
              {loading && <MyLoader />}
              {Object.keys(data).length > 0 && (
                <FlatList
                  data={this.state.news}
                  renderItem={this.renderItem}
                  keyExtractor={item => item._id}
                />
              )}
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  data: selectData,
  loading: selectLoading,
  selectBlog,
  blogtype: selectBlogCategory,
});
const mapDispatchToProps = { newsData, blogData, blogCategoryData };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewsScreen);
