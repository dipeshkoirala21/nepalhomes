/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';

class index extends Component {
  _handleBack = () => {
    this.props.navigation.navigate('Userprofile');
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ marginHorizontal: 10, marginTop: 40 }}>

          <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>
            <TouchableOpacity
              onPress={this._handleBack}>
              <Image
                style={{ height: 25, width: 25, marginTop: 20 }}
                source={require('../../../assets/left_blue.png')}
              />
            </TouchableOpacity>
            <Text
              style={{
                marginTop: 20,
                marginLeft: 20,
                color: '#3F51B5',
                fontWeight: 'bold',
                fontSize: 15,
              }}
            >
              Terms and Conditions
            </Text>
          </View>
          <Text style={{ color: '#1C3D5A', fontSize: 18, justifyContent: 'center', alignItems: 'center', marginTop: 20, }}> These terms and conditions outline the rules and regulations for the use of Company Name's Website, located at Website.com.

  By accessing this website we assume you accept these terms and conditions. Do not continue to use Website Name if you do not agree to take all of the terms and conditions stated on this page.

The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: “Client”, “You” and “Your” refers to you, the person log on this website and compliant to the Company's terms and conditions. “The Company”, “Ourselves”, “We”, “Our” and “Us”, refers to our Company. “Party”, “Parties”, or “Us”, refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client's needs in respect of provision of the Company's stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are t</Text>
        </View>
      </View>
    );
  }
}

export default index;
