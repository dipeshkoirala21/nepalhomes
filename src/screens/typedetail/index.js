/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Share } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import SafeAreaView from 'react-native-safe-area-view';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import {
  selectDetailData,
  selectFavouriteData,
} from '../../redux/property/property.selectors';
import {
  detailPropertyData,
  favouritePropertyData,
  favouritePropertyDataGet,
} from '../../redux/property/property.actions';
import { WebView } from 'react-native-webview';

import tempImg3 from '../../../assets/home.png';
import { IMAGE_URL } from '../../api';

class TypeDetail extends Component {
  state = {
    is_favourite: false,
    property_id: '',
  };
  componentDidMount = async () => {
    await this.props.detailPropertyData(this.props.navigation.getParam('slug'));
    const data = await this.props.favouritePropertyDataGet(
      this.props.navigation.getParam('id'),
    );
    //console.log(data.data.is_favourite);
    this.setState({
      is_favourite: data.data.is_favourite,
      property_id: this.props.navigation.getParam('id'),
    });
    //  console.log(response, 'dasdas');
  };
  onShare = async () => {
    try {
      const result = await Share.share({
        message: `https://nh.wafttech.com/detail/${this.props.navigation.getParam(
          'slug',
        )}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  onFavourite = () => {
    this.setState(
      {
        is_favourite: !this.state.is_favourite,
      },
      () => {
        this.props.favouritePropertyData(this.state);
      },
    );
  };
  render() {
    const { data } = this.props;
    //console.log(this.props.data);
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {/* <View
          style={{
            height: 300,
            width: '100%',
            backgroundColor: '#fff',
            position: 'absolute',
            top: 0,
          }}
        >
          <Text>bottom</Text>
        </View> */}
        <WebView
          source={{
            uri: `https://nh.wafttech.com/property/mobile/${this.props.navigation.getParam(
              'slug',
            )}`,
          }}
          originWhitelist={['*']}
          textZoom={100}
          containerStyle={{
            marginTop: 10,
            marginHorizontal: 10,
          }}
          showsVerticalScrollIndicator={false}
        />
        <View
          style={{
            height: 60,
            width: '100%',
            backgroundColor: '#fff',
            position: 'absolute',
            bottom: 0,
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
          }}
        >
          <TouchableOpacity
            style={{ height: 20, width: '25%', alignItems: 'center' }}
            onPress={this.onFavourite}
          >
            <Image
              style={{
                height: 20,
                width: 20,
              }}
              source={
                this.state.is_favourite === false
                  ? require('../../../assets/favourite.png')
                  : require('../../../assets/favourite_red.png')
              }
            />
            <Text style={{ fontSize: 15 }}>Favourite</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ height: 20, width: '25%', alignItems: 'center' }}
            onPress={this.onShare}
          >
            <Image
              style={{
                height: 20,
                width: 20,
              }}
              source={require('../../../assets/offer.png')}
            />
            <Text style={{ fontSize: 15 }}>Make Offer</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{ height: 20, width: '25%', alignItems: 'center' }}
            onPress={this.onShare}
          >
            <Image
              style={{
                height: 20,
                width: 20,
              }}
              source={require('../../../assets/comment.png')}
            />
            <Text style={{ fontSize: 15 }}>Comments</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ height: 20, width: '25%', alignItems: 'center' }}
            onPress={this.onShare}
          >
            <Image
              style={{
                height: 20,
                width: 20,
              }}
              source={require('../../../assets/share.png')}
            />
            <Text style={{ fontSize: 15 }}>Share</Text>
          </TouchableOpacity>
        </View>
        {/* <ScrollView>
          <View>
            <Image
              style={{
                height: 300,
                width: '100%',
                marginTop: 0,
                shadowOpacity: 3,
                shadowRadius: 4,
              }}
              source={
                data.media
                  ? {
                    uri: `${IMAGE_URL}${data.media.images[0].id.path}`,
                  }
                  : tempImg3
              }
            />
            <View
              style={{
                position: 'absolute',
                marginTop: 40,
                width: '100%',
                top: 0,
                left: 0,
                paddingHorizontal: 16,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Explore')}
                  style={{ height: 30, width: 30, left: 0 }}
                >
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                    }}
                    source={require('../../../assets/left_white.png')}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  style={{ height: 30, width: 30, right: 0 }}
                  onPress={this.onShare}
                >
                  <Image
                    style={{
                      height: 20,
                      width: 20,
                    }}
                    source={require('../../../assets/share.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ marginHorizontal: 20, marginTop: 20 }}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  height: 21,
                }}
              >
                <Image
                  style={{
                    height: '100%',
                    width: 16,
                  }}
                  source={require('../../../assets/location.png')}
                />
                <Text
                  style={{
                    left: 10,
                    color: '#000',
                    fontSize: 16,
                    fontWeight: 'bold',
                  }}
                >
                  {data.address && data.address.area_id.name
                    ? data.address.area_id.name
                    : 'loading'}
                  {', '}
                  {data.address && data.address.city_id.name
                    ? data.address.city_id.name
                    : 'loading'}
                </Text>
              </View>
              <TouchableOpacity
                style={{
                  height: 30,
                  backgroundColor: '#fff', // data.property_purpose === '123' ? '' : '',
                  borderColor: '#202B8B',
                  borderWidth: 1,
                  borderRadius: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingHorizontal: 2,
                }}
              >
                <Text style={{ color: '#202B8B' }}>
                  {data.basic && data.basic.property_purpose.title
                    ? data.basic.property_purpose.title
                    : 'loading'}
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 30,
              }}
            >
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 30,
                  }}
                >
                  <Image
                    style={{
                      height: '100%',
                      width: 33,
                    }}
                    source={require('../../../assets/bedroom.png')}
                  />
                  <Text
                    style={{
                      left: 10,
                      color: '#202B8B',
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}
                  >
                    {data.building && data.building.no_of.bedroom
                      ? data.building && data.building.no_of.bedroom
                      : 'loading'}
                  </Text>
                </View>
                <Text style={{ fontSize: 12, color: '#666666' }}>BEDROOM</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 30,
                  }}
                >
                  <Image
                    style={{
                      height: '100%',
                      width: 33,
                    }}
                    source={require('../../../assets/kitchen.png')}
                  />
                  <Text
                    style={{
                      left: 10,
                      color: '#202B8B',
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}
                  >
                    {data.building && data.building.no_of.kitchen
                      ? data.building.no_of.kitchen
                      : 'loading'}
                  </Text>
                </View>
                <Text style={{ fontSize: 12, color: '#666666' }}>KITCHEN</Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 35,
                  }}
                >
                  <Image
                    style={{
                      height: '100%',
                      width: 33,
                    }}
                    source={require('../../../assets/living.png')}
                  />
                  <Text
                    style={{
                      left: 10,
                      color: '#202B8B',
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}
                  >
                    {data.building && data.building.no_of.hall
                      ? data.building.no_of.hall
                      : 'loading'}
                  </Text>
                </View>
                <Text style={{ fontSize: 12, color: '#666666' }}>
                  LIVING ROOM
                </Text>
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    height: 35,
                  }}
                >
                  <Image
                    style={{
                      height: '100%',
                      width: 33,
                    }}
                    source={require('../../../assets/bath.png')}
                  />
                  <Text
                    style={{
                      left: 10,
                      color: '#202B8B',
                      fontSize: 16,
                      fontWeight: 'bold',
                    }}
                  >
                    {data.building && data.building.no_of.bathroom
                      ? data.building.no_of.bathroom
                      : 'loading'}
                  </Text>
                </View>
                <Text style={{ fontSize: 12, color: '#666666' }}>BATHROOM</Text>
              </View>
            </View>

            <View
              style={{
                width: '100%',
                backgroundColor: 'white',
                borderColor: '#d3d3d3',
                marginTop: 40,
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                  <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                    TOTAL AREA
                  </Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {data.location_property && data.location_property.total_area
                      ? data.location_property.total_area
                      : 'loading'}{' '}
                    {data.location_property &&
                      data.location_property.total_area_unit.title
                      ? data.location_property.total_area_unit.title
                      : 'loading'}
                  </Text>
                </View>
                <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                  <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                    HOUSE AREA
                  </Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {data.location_property && data.location_property.built_area
                      ? data.location_property.built_area
                      : 'loading'}{' '}
                    {data.location_property &&
                      data.location_property.built_area_unit.title
                      ? data.location_property.built_area_unit.title
                      : 'loading'}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                  <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                    BUILD YEAR
                  </Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {' '}
                    {data.building && data.building.built_year
                      ? data.building.built_year
                      : 'loading'}
                  </Text>
                </View>
                <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                  <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                    PARKING
                  </Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {data.building && data.building.parking
                      ? data.building.parking
                      : 'loading'}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                  <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                    CONDITION
                  </Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>New</Text>
                </View>
                <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                  <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                    FACE To
                  </Text>
                  <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                    {data.location_property &&
                      data.location_property.property_face.title
                      ? data.location_property.property_face.title
                      : 'loading'}
                  </Text>
                </View>
              </View>
              <View style={{ marginHorizontal: 10, marginBottom: 20 }}>
                <Text style={{ color: 'grey', fontSize: 18, marginTop: 10 }}>
                  ROAD ACCESS
                </Text>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>
                  {data.location_property &&
                    data.location_property.road_access_value
                    ? data.location_property.road_access_value
                    : 'loading'}{' '}
                  {data.location_property &&
                    data.location_property.road_access_length_unit.title
                    ? data.location_property.road_access_length_unit.title
                    : 'loading'}
                  {'/'}
                  {data.location_property &&
                    data.location_property.road_access_road_type.title
                    ? data.location_property.road_access_road_type.title
                    : 'loading'}
                </Text>
              </View>
            </View>
            <View>
              <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 10 }}>
                {' '}
                Description{' '}
              </Text>
              <Text>
                {' '}
                A house having 3 story, made on 9 anna land on sale at Baluwatar
                Kathmandu. The house is adjoining with 20 ft black top road, it
                has 4/5 car parking spaces, facing east. The ground floor of the
                house has 2 bed rooms (master bed room has attached bathroom),
                living room, dining and kitchen area, and one common bathroom.
                The first floor has a living room, specious dining area,
                kitchen, one common bathroom and a bed room. The top floor has a
                specious family room, 2 bed rooms (one master bed room with
                attached bathrooms), and one common bathroom.{' '}
              </Text>
            </View>
            <View style={{ marginBottom: 20 }}>
              <Text style={{ fontSize: 20, fontWeight: 'bold', marginTop: 20 }}>
                {' '}
                Seller Information{' '}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                  marginTop: 20,
                }}
              >
                <Image
                  style={{
                    height: 80,
                    width: 80,
                    backgroundColor: 'white',
                    borderRadius: 40,
                  }}
                  source={require('../../../assets/download.png')}
                />
                <View>
                  <Text style={{ fontSize: 16 }}> nepalhomesearch.com </Text>
                  <Text style={{ fontSize: 16, color: '#00BAF7' }}>
                    {' '}
                    Number One Real State Pvt. Ltd.{' '}
                  </Text>
                  <Text style={{ fontSize: 16 }}>9802070333</Text>
                </View>
              </View>
            </View>
          </View>
        </ScrollView> */}
      </SafeAreaView>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: selectDetailData,
  selectFavouriteData,
});
const mapDispatchToProps = {
  detailPropertyData,
  favouritePropertyData,
  favouritePropertyDataGet,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TypeDetail);
