/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, SafeAreaView, ScrollView, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Linking } from 'expo';

export class menu extends Component {
  handleEmail = () => {
    Linking.openURL('mailto:info@nepalhomes.com');
  }
  handleCall = () => {
    Linking.openURL('tel:1800 572 5050');
  }
  render() {
    return (
      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#202B8B',
        }}
      >
        <View
          style={{
            borderWidth: 0.5,
            borderColor: '#202B8B',
            borderRadius: 20,
            flex: 1,
            backgroundColor: '#F4F4F7',
            bottom: 10,
          }}
        >
          <ScrollView>
            <View
              style={{
                marginTop: 80,
                flex: 1,
                marginHorizontal: 10,
              }}
            >
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  style={{ height: 35, width: 250, marginTop: 10 }}
                  source={require('../../../assets/logo.png')}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                  height: 80,
                  width: '100%',
                  backgroundColor: '#fff',
                  borderRadius: 5,
                  marginTop: 80,
                  elevation: 3,
                  bottom: 10,
                }}
              >
                <Image
                  style={{ height: 70, width: 70, marginTop: 10, marginLeft: 10 }}
                  source={require('../../../assets/email.png')}
                />
                <View style={{ marginLeft: 20 }}>
                  <TouchableOpacity
                    onPress={this.handleEmail}>
                    <Text
                      style={{
                        fontSize: 17,
                        fontWeight: 'normal',
                        color: '#202B8B',
                      }}
                    >
                      Need any help? Write to us
                  </Text>
                    <Text
                      style={{
                        fontSize: 17,
                        fontWeight: 'normal',
                        color: '#0291DD',
                      }}
                    >
                      info@nepalhomes.com
                  </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'flex-start',
                  height: 80,
                  width: '100%',
                  backgroundColor: '#fff',
                  borderRadius: 5,
                  marginTop: 10,
                  elevation: 3,
                  bottom: 10,
                }}
              >
                <Image
                  style={{ height: 70, width: 70, marginTop: 10, marginLeft: 10 }}
                  source={require('../../../assets/call.png')}
                />
                <View style={{ marginLeft: 20 }}>
                  <TouchableOpacity
                    onPress={this.handleCall}>
                    <Text
                      style={{
                        fontSize: 17,
                        fontWeight: 'normal',
                        color: '#202B8B',
                      }}
                    >
                      Toll Free Number
                  </Text>
                    <Text
                      style={{
                        fontSize: 17,
                        fontWeight: 'normal',
                        color: '#0291DD',
                      }}
                    >
                      1800 572 5050
                  </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#8A8A8F', marginTop: 20 }}>
                  <Text style={{ fontSize: 18, color: '#8A8A8F' }}>TOOLS</Text>
                </View>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('EMICalculator')}
                  style={{ marginTop: 20, borderBottomWidth: 1, borderBottomColor: '#D8D8D8', borderStyle: 'dashed' }}>
                  <Text style={{ color: '#202B8B', fontSize: 17, fontWeight: 'normal' }}>EMI Calculator</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginTop: 20, borderBottomWidth: 1, borderBottomColor: '#D8D8D8', borderStyle: 'dashed' }}
                  onPress={() => this.props.navigation.navigate('UnitConverter')}>
                  <Text style={{ color: '#202B8B', fontSize: 17, fontWeight: 'normal' }}>Unit Converter</Text>
                </TouchableOpacity>
              </View>
              <View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: '#8A8A8F', marginTop: 40 }}>
                  <Text style={{ fontSize: 18, color: '#8A8A8F' }}>ABOUT</Text>
                </View>
                <TouchableOpacity
                  style={{ marginTop: 20, borderBottomWidth: 1, borderBottomColor: '#D8D8D8', borderStyle: 'dashed' }}>
                  <Text style={{ color: '#202B8B', fontSize: 17, fontWeight: 'normal' }}>About NepalHomes.com</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{ marginTop: 20, borderBottomWidth: 1, borderBottomColor: '#D8D8D8', borderStyle: 'dashed' }}>
                  <Text style={{ color: '#202B8B', fontSize: 17, fontWeight: 'normal' }}>Rate This App</Text>
                </TouchableOpacity>
              </View>
              <View style={{ alignItems: 'center', flexDirection: 'row', justifyContent: 'space-around' }}>
                <Image
                  style={{ height: 30, width: 30, marginTop: 40 }}
                  source={require('../../../assets/facebook_f.png')}
                />
                <Image
                  style={{ height: 30, width: 30, marginTop: 40 }}
                  source={require('../../../assets/twitter.png')}
                />
                <Image
                  style={{ height: 30, width: 30, marginTop: 40 }}
                  source={require('../../../assets/instagram.png')}
                />
                <Image
                  style={{ height: 30, width: 30, marginTop: 40 }}
                  source={require('../../../assets/linkedin.png')}
                />
                <Image
                  style={{ height: 30, width: 30, marginTop: 40 }}
                  source={require('../../../assets/youtube.png')}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default menu;
