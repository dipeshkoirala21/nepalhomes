/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  TextInput,
  Switch,
  View,
} from 'react-native';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectToken } from '../../redux/app/app.selectors';
import { registerRequest } from '../../redux/auth/auth.actions';

class Register extends Component {
  state = {
    disabled: false,
    switchValue: false,
    name: '',
    email: '',
    mobile: '',
    password: '',
  };
  static getDerivedStateFromProps = nextProps => {
    // console.log('reached here');
    if (nextProps.token) {
      // console.log(nextProps.token, 'here');
      nextProps.navigation.navigate('Login');
    }
    return null;
  };

  toggleSwitch = value => {
    //onValueChange of the switch this function will be called
    this.setState({ switchValue: value });
    //state changes according to switch
    //which will result in re-render the text
  };
  handleSubmit = () => {
    this.props.registerRequest(this.state);
  };
  handleBack = () => {
    this.props.navigation.navigate('ProfileScreen');
  };
  handleChange = name => text => this.setState({ [name]: text });
  render() {
    return (
      <KeyboardAvoidingView
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 20,
          flex: 1,
          flexDirection: 'column',
          position: 'relative',
        }}
        behavior="height"
      >
        <View
          style={{
            justifyContent: 'flex-start',
            position: 'absolute',
            left: 20,
            top: 40,
          }}
        >
          <TouchableOpacity onPress={this.handleBack}>
            <Image
              style={{ width: 25, height: 25 }}
              source={require('../../../assets/cancel.png')}
            />
          </TouchableOpacity>
        </View>
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 50,
            padding: 10,
          }}
          value={this.state.name}
          onChangeText={this.handleChange('name')}
          placeholder={'First Name'}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 20,
            color: '#006395',
            padding: 10,
          }}
          value={this.state.email}
          onChangeText={this.handleChange('email')}
          placeholder={'Email'}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 20,
            color: '#006395',
            padding: 10,
          }}
          value={this.state.mobile}
          onChangeText={this.handleChange('mobile')}
          placeholder={'Mobile No.'}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 20,
            color: '#006395',
            padding: 10,
          }}
          value={this.state.password}
          onChangeText={this.handleChange('password')}
          placeholder={'Password'}
          secureTextEntry={true}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
          }}
        >
          <Text style={{ color: '#006395', fontSize: 15, marginTop: 40 }}>
            I agree to terms and conditions
          </Text>
          <Switch
            style={{ marginTop: 40 }}
            onValueChange={this.toggleSwitch}
            value={this.state.switchValue}
          />
        </View>
        {this.state.switchValue === false ? (
          <TouchableOpacity
            disabled={this.state.disabled}
            style={{
              marginTop: 20,
              width: 300,
              height: 40,
              backgroundColor: '#d3d3d3',
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: '#006395',
              borderWidth: 1,
              borderRadius: 4,
            }}
            onPress={this.handleSubmit}
          >
            <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
              SIGN UP
            </Text>
          </TouchableOpacity>
        ) : (
            <TouchableOpacity
              style={{
                marginTop: 20,
                width: 300,
                height: 40,
                backgroundColor: '#202B8B',
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#006395',
                borderWidth: 1,
                borderRadius: 4,
              }}
              onPress={this.handleSubmit}
            >
              <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
                SIGN UP
            </Text>
            </TouchableOpacity>
          )}
        <TouchableOpacity
          style={{
            marginTop: 50,
            width: 220,
            height: 30,
            backgroundColor: '#4267B2',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            flexDirection: 'row',
            borderColor: '#006395',
            borderWidth: 1,
            borderRadius: 4,
            elevation: 4,
          }}
        // onPress={this.handleSubmit}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={require('../../../assets/facebook.png')}
          />
          <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
            Continue with Facebook
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            marginTop: 30,
            width: 220,
            height: 30,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            flexDirection: 'row',
            borderColor: 'white',
            borderWidth: 1,
            borderRadius: 4,
            elevation: 4,
          }}
        // onPress={this.handleSubmit}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={require('../../../assets/google.png')}
          />
          <Text style={{ color: 'black', fontSize: 15, fontWeight: 'bold' }}>
            Sign in with Google
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  token: selectToken,
});

const mapDispatchToProps = { registerRequest };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);
