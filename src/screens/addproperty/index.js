/* eslint-disable prettier/prettier */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Button } from 'react-native';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { enumsData } from '../../redux/enums/enums.actions';
import { selectEnumsData } from '../../redux/enums/enums.selectors';
import { setPostPropertyValueBasic } from '../../redux/property/property.actions';
import { selectPropertyDataBasic } from '../../redux/property/property.selectors';
import { ScrollView } from 'react-native-gesture-handler';

class AddProperty extends Component {
  async componentDidMount() {
    try {
      await this.props.enumsData();
    } catch (error) {
      //do sth
      console.log(error);
    }
  }

  onPurposeSelected = id => {
    this.props.setPostPropertyValueBasic({
      key: 'property_purpose',
      value: id,
    });
  };
  onCategoriesSelected = id => {
    this.props.setPostPropertyValueBasic({
      key: 'property_category',
      value: id,
    });
  };
  onTypeSelected = id => {
    this.props.setPostPropertyValueBasic({
      key: 'property_type',
      value: id,
    });
  };
  render() {
    const { data, selectEnumsData } = this.props;
    //console.log(data);

    return (
      <ScrollView style={{ flex: 1 }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 20,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              marginTop: 50,
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Userprofile')}
            >
              <Image
                style={{ height: 25, width: 25 }}
                source={require('../../../assets/left_blue.png')}
              />
            </TouchableOpacity>
            <View style={{ flexDirection: 'column' }}>
              <Text
                style={{
                  marginLeft: 20,
                  color: '#3F51B5',
                  fontWeight: 'normal',
                  fontSize: 18,
                }}
              >
                Post Property
              </Text>
              <Text style={{ color: '#666666', fontSize: 10, marginLeft: 20 }}>
                Step 1 of 3
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 50,
            }}
          >
            <Button
              title="Next"
              onPress={() => this.props.navigation.navigate('AddProperty2')}
              style={{ width: 40, marginTop: 50, height: 20 }}
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
            height: 80,
            backgroundColor: '#fff',
            borderRadius: 5,
            marginTop: 30,
            elevation: 4,
            marginHorizontal: 10,
          }}
        >
          <View style={{ width: '85%', marginLeft: 10 }}>
            <Text
              style={{ fontSize: 14, fontWeight: 'normal', color: '#4A4A4A' }}
            >
              If you have any difficulty in filling the form,{'\n'}Call us at
              166000-99999 (Toll Free)
            </Text>
          </View>
          <View
            style={{
              backgroundColor: '#4CD964',
              width: '15%',
              height: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderBottomLeftRadius: 40,
              borderTopLeftRadius: 40,
              borderStartWidth: 1,
              borderBottomWidth: 1,
              borderColor: '#4CD964',
              borderTopRightRadius: 5,
              borderBottomEndRadius: 4,
            }}
          >
            <Image
              style={{ height: 30, width: 30 }}
              source={require('../../../assets/phone.png')}
            />
          </View>
        </View>
        <View style={{ marginTop: 20 }}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'normal',
              color: '#1C3D5A',
              marginTop: 10,
              marginHorizontal: 10,
            }}
          >
            PURPOSE
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginHorizontal: 10,
          }}
        >
          {selectEnumsData.property_purpose ? (
            selectEnumsData.property_purpose.map(each => (
              <TouchableOpacity
                key={each._id}
                onPress={() => this.onPurposeSelected(each._id)}
                style={{
                  height: 40,
                  backgroundColor: '#fff',
                  borderColor:
                    data.property_purpose === each._id ? '#0291DD' : '#202B8B',
                  borderWidth: 1,
                  borderRadius: 20,
                  marginTop: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 10,
                  paddingHorizontal: 10,
                  marginHorizontal: 10,
                }}
              >
                <Text style={{ color: '#202B8B' }}>{each.title}</Text>
              </TouchableOpacity>
            ))
          ) : (
              <Text>loading...</Text>
            )}
        </View>
        <View style={{ marginTop: 20 }}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'normal',
              color: '#1C3D5A',
              marginTop: 10,
              marginHorizontal: 10,
            }}
          >
            CATEGORIES
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginTop: 40,
            flexWrap: 'wrap',
          }}
        >
          {selectEnumsData.property_category ? (
            selectEnumsData.property_category.map(each => (
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 10,
                  marginBottom: 20,
                }}
                key={each._id}
              >
                <TouchableOpacity
                  style={{
                    height: 100,
                    width: 100,
                    borderWidth: 1,
                    borderRadius: 50,
                    backgroundColor: '#F4F4F7',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor:
                      data.property_category === each._id
                        ? '#0291DD'
                        : '#d3d3d3',
                  }}
                  onPress={() => this.onCategoriesSelected(each._id)}
                >
                  <Image
                    style={{
                      height: 50,
                      width: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    source={require('../../../assets/home.png')}
                  />
                </TouchableOpacity>
                <Text style={{ color: '#202B8B' }}>{each.title}</Text>
              </View>
            ))
          ) : (
              <Text>loading...</Text>
            )}
        </View>
        <View style={{ marginTop: 20 }}>
          <Text
            style={{
              fontSize: 18,
              fontWeight: 'normal',
              color: '#1C3D5A',
              marginTop: 10,
              marginHorizontal: 10,
            }}
          >
            PROPERTY TYPE
          </Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginTop: 40,
            flexWrap: 'wrap',
          }}
        >
          {selectEnumsData.property_type ? (
            selectEnumsData.property_type.map(each => (
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 10,
                  marginBottom: 20,
                }}
                key={each._id}
              >
                <TouchableOpacity
                  style={{
                    height: 100,
                    width: 100,
                    borderWidth: 1,
                    borderRadius: 50,
                    backgroundColor: '#F4F4F7',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor:
                      data.property_type === each._id
                        ? '#0291DD'
                        : '#d3d3d3',
                  }}
                  onPress={() => this.onTypeSelected(each._id)}
                >
                  <Image
                    style={{
                      height: 50,
                      width: 50,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    source={require('../../../assets/home.png')}
                  />
                </TouchableOpacity>
                <Text style={{ color: '#202B8B' }}>{each.title}</Text>
              </View>
            ))
          ) : (
              <Text>loading...</Text>
            )}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  data: selectPropertyDataBasic,
  selectEnumsData,
});
const mapDispatchToProps = { enumsData, setPostPropertyValueBasic };

export default connect(mapStateToProps, mapDispatchToProps)(AddProperty);
