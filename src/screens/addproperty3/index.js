/* eslint-disable no-alert */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  Button,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Picker,
  Modal,
  Dimensions,
  KeyboardAvoidingView,
} from 'react-native';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import { IMAGE_URL } from '../../api';
import tempImg3 from '../../../assets/home.png';
import { enumsData } from '../../redux/enums/enums.actions';
import { selectEnumsData } from '../../redux/enums/enums.selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { setPostPropertyValueLocationProperty } from '../../redux/property/property.actions';
import {
  selectLocationPropertyData,
  selectPropertyDataBuilding,
  selectMediaData,
  selectPropertyDataBasic,
  selectPropertyData,
  selectPropertyTags,
} from '../../redux/property/property.selectors';
import {
  setPostPropertyValueBuilding,
  setPostPropertyValueMedia,
  setPostPropertyValueBasic,
  postPropertyData,
  setPostPropertyValueTags,
} from '../../redux/property/property.actions';
import { mediaUpload } from '../../redux/property/property.actions';

class AddProperty3 extends Component {
  state = {
    modalVisible: false,
    showCamera: false,
    image: null,
    tags: '',
    url: '',
  };
  async componentDidMount() {
    try {
      await this.props.enumsData();
    } catch (error) {
      //do sth
      console.log(error);
    }
  }
  handleGallery = async () => {
    // const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
    // console.log(permission, 'camera roll permission');
    // if (permission.status === 'granted') {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    console.log(result, 'hello');

    if (result.cancelled) {
      //not granted
      console.log('not granted');
      alert('Sorry, we need camera roll permissions to make this work!');
      return;
    }
    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
    const splits = result.uri.split('/');
    const name = splits[splits.length - 1];
    this.props.mediaUpload({
      uri: result.uri,
      name,
      type: name.substr(name.length - 3) === 'png' ? 'image/png' : 'image/jpeg',
    });
    this.setModalVisible(false);
  };

  handleCamera = async () => {
    const permission = await Permissions.getAsync(
      Permissions.CAMERA,
      Permissions.CAMERA_ROLL,
    );
    console.log(permission, 'camera roll permission');
    if (permission.status === 'granted') {
      let result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });

      // console.log(result, 'hello');

      if (!result.cancelled) {
        this.setState({ image: result.uri });
      }
      const splits = result.uri.split('/');
      const name = splits[splits.length - 1];
      this.props.mediaUpload({
        uri: result.uri,
        name,
        type:
          name.substr(name.length - 3) === 'png' ? 'image/png' : 'image/jpeg',
      });
      this.setModalVisible(false);
    } else if (permission.status !== 'granted') {
      const newPermission = await Permissions.askAsync(
        Permissions.CAMERA,
        Permissions.CAMERA_ROLL,
      );
      if (newPermission.status === 'granted') {
        //its granted.
        console.log('grantted');
        let result = await ImagePicker.launchCameraAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });

        if (!result.cancelled) {
          this.setState({ image: result.uri });
        }
        const splits = result.uri.split('/');
        const name = splits[splits.length - 1];
        this.props.mediaUpload({
          uri: result.uri,
          name,
          type:
            name.substr(name.length - 3) === 'png' ? 'image/png' : 'image/jpeg',
        });
        this.setModalVisible(false);
      }
    } else {
      //not granted
      console.log('not granted');
      alert('Sorry, we need camera roll permissions to make this work!');
    }
  };

  setModalVisible = visible => {
    this.setState({ modalVisible: visible });
  };
  handleChange = name => text => this.setState({ [name]: text });

  onAmenitiesSelected = id => {
    // console.log(id, 'console');
    const {
      selectPropertyDataBuilding: { amenities },
    } = this.props;
    if (amenities.includes(id)) {
      this.props.setPostPropertyValueBuilding({
        key: 'amenities',
        value: amenities.filter(each => each !== id),
      });
    } else {
      this.props.setPostPropertyValueBuilding({
        key: 'amenities',
        value: [...amenities, id],
      });
    }
  };
  onTagsSelected = () => {
    const value = this.state.tags;
    const {
      selectPropertyData: { tags },
    } = this.props;
    // console.log(typeof tags);
    // console.log('herer', tags);

    if (tags.includes(value)) {
      // this.props.setPostPropertyValueTags({
      //   key: 'tags',
      //   value: tags.filter(each => each !== value),
      // });
      return null;
    } else {
      this.props.setPostPropertyValueTags([...tags, value]);
    }
    this.setState({ tags: '' });
  };
  onPropertyLocationValueSelected = (id, key) => {
    this.props.setPostPropertyValueLocationProperty({
      key,
      value: id,
    });
  };
  onBuildingSelected = (key, value) => {
    //console.log(value, key, 'value, key');
    this.props.setPostPropertyValueBuilding({
      // key: 'property_purpose',
      key,
      value,
    });
  };
  onMediaSelected = () => {
    const link = this.state.url;
    const Url = link.split('/');
    const name = Url[Url.length - 1];
    // console.log(name);
    // console.log(value, key, 'value, key');
    this.props.setPostPropertyValueMedia({
      key: 'youtube_video_id',
      value: name,
    });
  };
  onBasicSelected = (key, value) => {
    //console.log(value, key, 'value, key');
    this.props.setPostPropertyValueBasic({
      // key: 'property_purpose',
      key,
      value,
    });
  };
  onPostNow = () => {
    const link = this.state.url;
    const Url = link.split('/');
    const name = Url[Url.length - 1];

    const image_id = this.props.selectPropertyData.media.images.map(
      function image(each) {
        return {
          id: each.id._id,
          caption: each.caption,
        };
      },
    );
    let main_data = {};
    main_data = {
      ...this.props.selectPropertyData,
      media: {
        images: [...image_id],
        youtube_video_id: name,
      },
    };
    console.log(main_data);
    this.props.postPropertyData(main_data);
    this.props.navigation.navigate('Userprofile');
  };

  render() {
    const {
      data,
      selectEnumsData,
      selectPropertyDataBuilding,
      selectPropertyDataBasic,
      media,
      selectPropertyTags,
    } = this.props;
    const AD = [
      { name: 'January', id: 1 },
      { name: 'February', id: 2 },
      { name: 'March', id: 3 },
      { name: 'April', id: 4 },
      { name: 'May', id: 5 },
      { name: 'June', id: 6 },
      { name: 'July', id: 7 },
      { name: 'August', id: 8 },
      { name: 'September', id: 9 },
      { name: 'October', id: 10 },
      { name: 'November', id: 11 },
      { name: 'December', id: 12 },
    ];
    const BS = [
      { name: 'Baishak', id: 1 },
      { name: 'Jestha', id: 2 },
      { name: 'Ashad', id: 3 },
      { name: 'Shrawan', id: 4 },
      { name: 'Bhadra', id: 5 },
      { name: 'Ashwin', id: 6 },
      { name: 'Kartik', id: 7 },
      { name: 'Mangshir', id: 8 },
      { name: 'Poush', id: 9 },
      { name: 'Magh', id: 10 },
      { name: 'Falgun', id: 11 },
      { name: 'Chaitra', id: 12 },
    ];
    return (
      <KeyboardAvoidingView
        style={{ flex: 1, bottom: 10 }}
        behavior="padding"
        enabled
      >
        <ScrollView>
          <View style={{ marginHorizontal: 20, flex: 1, marginBottom: 20 }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 50,
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('AddProperty2')}
                >
                  <Image
                    style={{ height: 25, width: 25 }}
                    source={require('../../../assets/left_blue.png')}
                  />
                </TouchableOpacity>
                <View style={{ flexDirection: 'column' }}>
                  <Text
                    style={{
                      marginLeft: 20,
                      color: '#3F51B5',
                      fontWeight: 'normal',
                      fontSize: 18,
                    }}
                  >
                    Post Property
                  </Text>
                  <Text
                    style={{ color: '#666666', fontSize: 10, marginLeft: 20 }}
                  >
                    Step 3 of 3
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 50,
                }}
              >
                <Button
                  title="POST NOW"
                  onPress={this.onPostNow}
                  style={{ width: 80, marginTop: 50, height: 20 }}
                />
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
                height: 80,
                backgroundColor: '#fff',
                borderRadius: 5,
                marginTop: 30,
                elevation: 4,
              }}
            >
              <View style={{ width: '85%', marginLeft: 10 }}>
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: 'normal',
                    color: '#4A4A4A',
                  }}
                >
                  If you have any difficulty in filling the form,{'\n'}Call us
                  at 166000-99999 (Toll Free)
                </Text>
              </View>
              <View
                style={{
                  backgroundColor: '#4CD964',
                  width: '15%',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderBottomLeftRadius: 40,
                  borderTopLeftRadius: 40,
                  borderStartWidth: 1,
                  borderBottomWidth: 1,
                  borderColor: '#4CD964',
                  borderTopRightRadius: 5,
                  borderBottomEndRadius: 4,
                }}
              >
                <Image
                  style={{ height: 30, width: 30 }}
                  source={require('../../../assets/phone.png')}
                />
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                TOTAL AREA
              </Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <TextInput
                style={{
                  height: 52,
                  width: 200,
                  borderWidth: 1,
                  borderColor: '#979797',
                  borderRadius: 5,
                  padding: 10,
                }}
                value={data.total_area}
                onChangeText={text =>
                  this.onPropertyLocationValueSelected(text, 'total_area')
                }
                placeholder={'Total Area'}
              />
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                }}
              >
                <Picker
                  selectedValue={data.total_area_unit}
                  style={{
                    width: 150,
                    height: 50,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onPropertyLocationValueSelected(
                      itemValue,
                      'total_area_unit',
                    )
                  }
                >
                  {selectEnumsData.area_unit ? (
                    selectEnumsData.area_unit.map(each => (
                      <Picker.Item
                        label={each.title}
                        value={each._id}
                        key={each._id}
                      />
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </Picker>
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                BUILT UP AREA
              </Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <TextInput
                style={{
                  height: 52,
                  width: 200,
                  borderWidth: 1,
                  borderColor: '#979797',
                  borderRadius: 5,
                  padding: 10,
                }}
                value={data.built_area}
                onChangeText={text =>
                  this.onPropertyLocationValueSelected(text, 'built_area')
                }
                placeholder={'Built Area'}
              />
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                }}
              >
                <Picker
                  selectedValue={data.built_area_unit}
                  style={{
                    width: 150,
                    height: 50,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onPropertyLocationValueSelected(
                      itemValue,
                      'built_area_unit',
                    )
                  }
                >
                  {selectEnumsData.area_unit ? (
                    selectEnumsData.area_unit.map(each => (
                      <Picker.Item
                        label={each.title}
                        value={each._id}
                        key={each._id}
                      />
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </Picker>
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                PROPERTY FACE
              </Text>
            </View>
            <View
              style={{
                borderRadius: 5,
                borderColor: '#979797',
                borderWidth: 1,
                marginTop: 10,
              }}
            >
              <Picker
                selectedValue={data.property_face}
                style={{
                  width: '100%',
                  height: 50,
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.onPropertyLocationValueSelected(
                    itemValue,
                    'property_face',
                  )
                }
              >
                {selectEnumsData.property_face ? (
                  selectEnumsData.property_face.map(each => (
                    <Picker.Item
                      label={each.title}
                      value={each._id}
                      key={each._id}
                    />
                  ))
                ) : (
                    <Text>loading...</Text>
                  )}
              </Picker>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                ROAD ACCESS
              </Text>
            </View>
            <View
              style={{
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <TextInput
                style={{
                  height: 52,
                  width: 200,
                  borderWidth: 1,
                  borderColor: '#979797',
                  borderRadius: 5,
                  padding: 10,
                }}
                value={data.road_access_value}
                onChangeText={text =>
                  this.onPropertyLocationValueSelected(
                    text,
                    'road_access_value',
                  )
                }
                placeholder={'Road Access'}
              />
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                }}
              >
                <Picker
                  selectedValue={data.road_access_length_unit}
                  style={{
                    width: 150,
                    height: 50,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onPropertyLocationValueSelected(
                      itemValue,
                      'road_access_length_unit',
                    )
                  }
                >
                  {selectEnumsData.length_unit ? (
                    selectEnumsData.length_unit.map(each => (
                      <Picker.Item
                        label={each.title}
                        value={each._id}
                        key={each._id}
                      />
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </Picker>
              </View>
            </View>
            <View
              style={{
                borderRadius: 5,
                borderColor: '#979797',
                borderWidth: 1,
                marginTop: 10,
              }}
            >
              <Picker
                selectedValue={data.road_access_road_type}
                style={{
                  width: '100%',
                  height: 50,
                }}
                onValueChange={(itemValue, itemIndex) =>
                  this.onPropertyLocationValueSelected(
                    itemValue,
                    'road_access_road_type',
                  )
                }
              >
                {selectEnumsData.road_type ? (
                  selectEnumsData.road_type.map(each => (
                    <Picker.Item
                      label={each.title}
                      value={each._id}
                      key={each._id}
                    />
                  ))
                ) : (
                    <Text>loading...</Text>
                  )}
              </Picker>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <View style={{ marginTop: 20 }}>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'normal',
                    color: '#1C3D5A',
                    marginTop: 10,
                  }}
                >
                  BUILT YEAR
                </Text>
                <TextInput
                  style={{
                    height: 50,
                    width: '100%',
                    borderWidth: 1,
                    borderColor: '#979797',
                    borderRadius: 5,
                    padding: 10,
                  }}
                  value={selectPropertyDataBuilding.built_year}
                  onChangeText={text =>
                    this.onBuildingSelected('built_year', text)
                  }
                  placeholder={'Year'}
                />
              </View>
              <View
                style={{
                  borderRadius: 5,
                  borderColor: '#979797',
                  borderWidth: 1,
                  width: 100,
                  marginTop: 57,
                }}
              >
                <Picker
                  selectedValue={selectPropertyDataBuilding.calender_type}
                  style={{
                    width: 100,
                    height: 50,
                  }}
                  onValueChange={(itemValue, itemIndex) =>
                    this.onBuildingSelected('calender_type', itemValue)
                  }
                >
                  {selectEnumsData.calender_type ? (
                    selectEnumsData.calender_type.map(each => (
                      <Picker.Item
                        label={each.title}
                        value={each._id}
                        key={each._id}
                      />
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </Picker>
              </View>
              <View>
                <View style={{ marginTop: 20 }}>
                  <Text
                    style={{
                      fontSize: 18,
                      fontWeight: 'normal',
                      color: '#1C3D5A',
                      marginTop: 10,
                    }}
                  >
                    BUILT MONTH
                  </Text>
                </View>

                <View
                  style={{
                    borderRadius: 5,
                    borderColor: '#979797',
                    borderWidth: 1,
                    width: 150,
                  }}
                >
                  <Picker
                    selectedValue={selectPropertyDataBuilding.built_month}
                    style={{
                      width: 150,
                      height: 50,
                    }}
                    onValueChange={(itemValue, itemIndex) =>
                      this.onBuildingSelected('built_month', itemValue)
                    }
                  >
                    {selectPropertyDataBuilding.calender_type ===
                      '5d6cc52873552113c0396023'
                      ? BS.map((each, index) => (
                        <Picker.Item
                          label={each.name}
                          value={each.id}
                          key={each.id}
                        />
                      ))
                      : AD.map(each => (
                        <Picker.Item
                          label={each.name}
                          value={each.id}
                          key={each.id}
                        />
                      ))}
                  </Picker>
                </View>
              </View>
            </View>
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                  marginHorizontal: 10,
                }}
              >
                AMENITIES
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                marginHorizontal: 10,
              }}
            >
              {selectEnumsData.amenities ? (
                selectEnumsData.amenities.map(each => (
                  <TouchableOpacity
                    key={each._id}
                    onPress={() => this.onAmenitiesSelected(each._id)}
                    style={{
                      height: 40,
                      backgroundColor: '#fff',
                      borderColor: selectPropertyDataBuilding.amenities.includes(
                        each._id,
                      )
                        ? '#0291DD'
                        : '#202B8B',
                      borderWidth: 1,
                      borderRadius: 20,
                      marginTop: 20,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 10,
                      paddingHorizontal: 10,
                      marginHorizontal: 10,
                    }}
                  >
                    <Text style={{ color: '#202B8B' }}>{each.title}</Text>
                  </TouchableOpacity>
                ))
              ) : (
                  <Text>loading...</Text>
                )}
            </View>
            <View>
              <TouchableOpacity
                onPress={() => this.setModalVisible(true)}
                style={{
                  height: 100,
                  backgroundColor: '#fff',
                  borderColor: '#d3d3d3',
                  borderWidth: 1,
                  borderStyle: 'dotted',
                  borderRadius: 5,
                  marginTop: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginRight: 10,
                  paddingHorizontal: 10,
                  marginHorizontal: 10,
                }}
              >
                <Image
                  style={{ height: 25, width: 25 }}
                  source={require('../../../assets/imageupload.png')}
                />
                <Text style={{ color: '#0291DD' }}>UPLOAD PHOTOS</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                marginHorizontal: 20,
                justifyContent: 'space-around',
                flexDirection: 'row',
                marginTop: 5,
              }}
            >
              {media.images ? (
                media.images.map(each => (
                  <Image
                    key={each.id._id}
                    style={{
                      height: 100,
                      width: 100,
                      borderWidth: 0.5,
                      borderColor: '#fff',
                      borderRadius: 5,
                    }}
                    source={
                      each && each.id.path
                        ? {
                          uri: `${IMAGE_URL}${each.id.path}`,
                        }
                        : tempImg3
                    }
                  />
                ))
              ) : (
                  <Text>loading...</Text>
                )}
            </View>
            <View>
              <Modal
                style={{ justifyContent: 'center' }}
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  this.setModalVisible(false);
                }}
              >
                <View
                  style={{
                    height: Dimensions.get('window').height,
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setModalVisible(false)}
                    style={{ flex: 1 }}
                  />
                  <View
                    style={{
                      backgroundColor: '#fff',
                      height: 170,
                      marginTop: 20,
                      marginHorizontal: 10,
                      borderWidth: 1,
                      borderColor: '#fff',
                      borderRadius: 5,
                      elevation: 8,
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        width: '100%',
                        height: 50,
                        backgroundColor: '#0291DD',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#0291DD',
                        borderWidth: 1,
                        borderRadius: 4,
                      }}
                      onPress={this.handleGallery}
                    >
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 15,
                          fontWeight: 'bold',
                        }}
                      >
                        Upload From Gallery
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: '100%',
                        height: 50,
                        backgroundColor: '#0291DD',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#0291DD',
                        borderWidth: 1,
                        borderRadius: 4,
                      }}
                      onPress={this.handleCamera}
                    >
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 15,
                          fontWeight: 'bold',
                        }}
                      >
                        Take Picture
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: '100%',
                        height: 50,
                        marginTop: 10,
                        backgroundColor: '#fff',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#C8C7CC',
                        borderWidth: 1,
                        borderRadius: 4,
                      }}
                      onPress={() => this.setModalVisible(false)}
                    >
                      <Text
                        style={{
                          color: '#FF3B30',
                          fontSize: 15,
                          fontWeight: 'bold',
                        }}
                      >
                        Cancel
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </Modal>
            </View>
            <View style={{ marginTop: 10 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                YOUTUBE VIDEO URL
              </Text>
            </View>
            <TextInput
              style={{
                height: 50,
                width: '100%',
                borderWidth: 1,
                borderColor: '#979797',
                borderRadius: 5,
                padding: 10,
                marginTop: 10,
              }}
              value={this.state.url}
              onChangeText={this.handleChange('url')}
              placeholder={'Video Url'}
            />
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                PROPERTY TITLE
              </Text>
            </View>
            <TextInput
              style={{
                height: 50,
                width: '100%',
                borderWidth: 1,
                borderColor: '#979797',
                borderRadius: 5,
                padding: 10,
                marginTop: 10,
              }}
              value={selectPropertyDataBasic.built_year}
              onChangeText={text => this.onBasicSelected('title', text)}
              placeholder={'Property Title'}
            />
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                DESCRIPTION
              </Text>
            </View>
            <TextInput
              style={{
                height: 150,
                width: '100%',
                borderWidth: 1,
                borderColor: '#979797',
                borderRadius: 5,
                padding: 10,
                marginTop: 10,
              }}
              value={selectPropertyDataBasic.description}
              onChangeText={text => this.onBasicSelected('description', text)}
              placeholder={'Description'}
            />
            <View style={{ marginTop: 20 }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'normal',
                  color: '#1C3D5A',
                  marginTop: 10,
                }}
              >
                TAGS
              </Text>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-around' }}
            >
              <TextInput
                style={{
                  height: 50,
                  width: '80%',
                  borderWidth: 1,
                  borderColor: '#979797',
                  borderRadius: 5,
                  padding: 10,
                  marginTop: 10,
                }}
                value={this.state.tags}
                onChangeText={this.handleChange('tags')}
                placeholder={'Tags'}
              />
              <TouchableOpacity
                style={{
                  marginTop: 10,
                  width: 60,
                  height: 50,
                  backgroundColor: '#0291DD',
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderColor: '#0291DD',
                  borderWidth: 1,
                  borderRadius: 4,
                }}
                onPress={this.onTagsSelected}
              >
                <Text
                  style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}
                >
                  Add
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-around' }}
            >
              {selectPropertyTags.map(each => (
                <TouchableOpacity
                  key={each}
                  style={{
                    marginTop: 10,
                    height: 40,
                    width: 60,
                    backgroundColor: '#d3d3d3',
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderColor: '#d3d3d3',
                    borderWidth: 1,
                    borderRadius: 10,
                  }}
                  activeOpacity={1}
                >
                  <Text style={{ color: '#000', fontSize: 10 }}>{each}</Text>
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  selectEnumsData,
  data: selectLocationPropertyData,
  selectPropertyDataBuilding,
  media: selectMediaData,
  selectPropertyDataBasic,
  selectPropertyData,
  selectPropertyTags,
});
const mapDispatchToProps = {
  enumsData,
  setPostPropertyValueLocationProperty,
  setPostPropertyValueBuilding,
  mediaUpload,
  setPostPropertyValueMedia,
  setPostPropertyValueBasic,
  postPropertyData,
  setPostPropertyValueTags,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddProperty3);
