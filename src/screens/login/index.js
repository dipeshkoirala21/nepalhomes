/* eslint-disable no-alert */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  KeyboardAvoidingView,
  Image,
  View,
  // Platform,
  // Linking,
} from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { loginRequest, facebook, google } from '../../redux/auth/auth.actions';
import { selectToken } from '../../redux/app/app.selectors';
// import SafariView from 'react-native-safari-view';
import * as GoogleSignIn from 'expo-google-sign-in';
import * as Facebook from 'expo-facebook';

class Login extends Component {
  state = {
    email: '',
    password: '',
    user: null,
    FBInitialized: false,
  };
  componentDidMount() {
    this.initAsync();
  }
  loginWithFacebook = async () => {
    try {
      await Facebook.initializeAsync('403635297248992', 'Nepal Homes');
      const { type, token } = await Facebook.logInWithReadPermissionsAsync(
        '403635297248992',
        {
          permissions: ['public_profile', 'email'],
        },
      );
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        // const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        this.props.facebook({
          data: { access_token: token },
          // navigate: this.props.navigation,
        });
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`error: ${message}`);
    }
  };
  initAsync = async () => {
    await GoogleSignIn.initAsync({
      clientId:
        '521974798939-0co9ftonu4570dsccfnqsmnt1sh5oi5k.apps.googleusercontent.com',
    });
    this._syncUserWithStateAsync();
  };

  _syncUserWithStateAsync = async () => {
    //alert('FUNC');

    const user = await GoogleSignIn.signInSilentlyAsync();
    this.setState({ user });
  };

  signOutAsync = async () => {
    await GoogleSignIn.signOutAsync();
    this.setState({ user: null });
  };

  signInAsync = async () => {
    try {
      let accessToken = '';
      await GoogleSignIn.askForPlayServicesAsync();
      const { type, user } = await GoogleSignIn.signInAsync();
      if (type === 'success') {
        this._syncUserWithStateAsync();
        accessToken = user.auth.accessToken;
      }
      if (accessToken) {
        this.props.google({
          data: { avatar: user.photoUrl, access_token: accessToken },
        });
      }
    } catch ({ message }) {
      alert('login: Error:' + message);
    }
  };
  static getDerivedStateFromProps = nextProps => {
    if (nextProps.token) {
      nextProps.navigation.navigate('Userprofile');
    }
    return null;
  };

  handleSubmit = () => {
    this.props.loginRequest(this.state);
  };

  handleChange = name => text => this.setState({ [name]: text });

  handleBack = () => {
    this.props.navigation.navigate('ProfileScreen');
  };
  render() {
    return (
      <KeyboardAvoidingView
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 50,
          flex: 1,
          position: 'relative',
        }}
        behavior="height"
      >
        <View
          style={{
            justifyContent: 'flex-start',
            position: 'absolute',
            left: 20,
            top: 20,
          }}
        >
          <TouchableOpacity onPress={this.handleBack}>
            <Image
              style={{ width: 25, height: 25 }}
              source={require('../../../assets/cancel.png')}
            />
          </TouchableOpacity>
        </View>
        <Image
          style={{ width: 80, height: 80 }}
          source={require('../../../assets/home.png')}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 80,
            padding: 10,
          }}
          value={this.state.email}
          onChangeText={this.handleChange('email')}
          placeholder={'Email'}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 20,
            color: '#006395',
            padding: 10,
          }}
          value={this.state.password}
          onChangeText={this.handleChange('password')}
          placeholder={'Password'}
          secureTextEntry={true}
        />
        <TouchableOpacity
          style={{
            marginTop: 20,
            width: 300,
            height: 40,
            backgroundColor: '#202B8B',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: '#006395',
            borderWidth: 1,
            borderRadius: 4,
          }}
          onPress={this.handleSubmit}
        >
          <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
            LOGIN
          </Text>
        </TouchableOpacity>

        <Text
          style={{ color: '#202B8B', fontSize: 15, marginTop: 40 }}
          onPress={() => this.props.navigation.navigate('ForgotPassword')}
        >
          FORGOT PASSWORD?
        </Text>
        <TouchableOpacity
          onPress={this.loginWithFacebook}
          style={{
            marginTop: 50,
            width: 220,
            height: 30,
            backgroundColor: '#4267B2',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            flexDirection: 'row',
            borderColor: '#006395',
            borderWidth: 1,
            borderRadius: 4,
            elevation: 4,
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={require('../../../assets/facebook.png')}
          />
          <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
            Continue with Facebook
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={this.signInAsync}
          style={{
            marginTop: 30,
            width: 220,
            height: 30,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            flexDirection: 'row',
            borderColor: 'white',
            borderWidth: 1,
            borderRadius: 4,
            elevation: 4,
          }}
        >
          <Image
            style={{ width: 20, height: 20 }}
            source={require('../../../assets/google.png')}
          />
          <Text style={{ color: 'black', fontSize: 15, fontWeight: 'bold' }}>
            Sign in with Google
          </Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  token: selectToken,
});

const mapDispatchToProps = { loginRequest, facebook, google };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
