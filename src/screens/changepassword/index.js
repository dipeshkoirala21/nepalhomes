/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  Image,
  TextInput,
  View,
} from 'react-native';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { changePasswordRequest } from '../../redux/auth/auth.actions';
import { selectToken } from '../../redux/app/app.selectors';

class ChangePassword extends Component {
  state = {
    oldPassword: '',
    newPassword: '',
    newPassword2: '',
  };
  // static getDerivedStateFromProps = nextProps => {
  //   // console.log('reached here');

  //   return null;
  // };
  handleSubmit = () => {
    this.props.changePasswordRequest(this.state).then(res => {
      this.props.navigation.navigate('Login');
    });
  };

  handleChange = name => text => this.setState({ [name]: text });
  handleNavigation = () => {
    this.props.navigation.navigate('Userprofile');
  };
  render() {
    return (
      <KeyboardAvoidingView
        style={{
          alignItems: 'center',
          marginTop: 40,
          flex: 1,
          flexDirection: 'column',
        }}
        behavior="height"
      >
        <Image
          style={{ width: 80, height: 80 }}
          source={require('../../../assets/home.png')}
        />
        <Text style={{ color: 'white', fontSize: 25, fontWeight: 'bold' }}>
          Change Password
        </Text>
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 30,
            padding: 10,
          }}
          value={this.state.oldPassword}
          onChangeText={this.handleChange('oldPassword')}
          placeholder={'Old Password'}
          secureTextEntry={true}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 20,
            color: '#006395',
            padding: 10,
          }}
          value={this.state.newPassword}
          onChangeText={this.handleChange('newPassword')}
          placeholder={'New Password'}
          secureTextEntry={true}
        />
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 20,
            color: '#006395',
            padding: 10,
          }}
          value={this.state.newPassword2}
          onChangeText={this.handleChange('newPassword2')}
          placeholder={'Confirm New Password'}
          secureTextEntry={true}
        />
        <View>
          <TouchableOpacity
            style={{
              marginTop: 20,
              width: 300,
              height: 40,
              backgroundColor: '#006395',
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: '#006395',
              borderWidth: 1,
              borderRadius: 4,
            }}
            onPress={this.handleSubmit}
          >
            <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
              Change Password
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginTop: 20,
              width: 300,
              height: 40,
              backgroundColor: '#006395',
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: '#006395',
              borderWidth: 1,
              borderRadius: 4,
            }}
            onPress={this.handleNavigation}
          >
            <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
              Back
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  token: selectToken,
});

const mapDispatchToProps = { changePasswordRequest };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChangePassword);
