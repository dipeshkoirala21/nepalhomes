/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, FlatList, Image, TouchableOpacity, View } from 'react-native';
import { IMAGE_URL } from '../../api';
import tempImg3 from '../../../assets/home.png';
import SafeAreaView from 'react-native-safe-area-view';
import { ScrollView } from 'react-native-gesture-handler';
import FilterModal from '../component/modal';

import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { selectFilterData } from '../../redux/property/property.selectors';
import { filterPropertyData } from '../../redux/property/property.actions';
import { setQueryData } from '../../redux/property/property.selectors';
import { setFilterDataValue } from '../../redux/property/property.actions';

//import ImageSlider from '../../global/components/ImageSlider';

export class Search extends Component {
  state = {
    modalVisible: false,
  };
  // componentWillMount() {
  //   this.props.navigation.getParam('filteredData');
  //   // console.log(this.props.navigation.getParam('filteredData'));
  //   this.setState({
  //     filter: this.props.navigation.getParam('filteredData'),
  //   });
  // }
  setModalVisible = visible => {
    this.setState({ modalVisible: visible });
  };
  formatToNepaliStyle(amt) {
    if (!amt) {
      return '0';
    }
    const amtStr = `${amt}`;
    const indexOfPeriod = amtStr.indexOf('.');
    let sliceIndex = indexOfPeriod - 1;
    if (indexOfPeriod === -1) {
      sliceIndex = amtStr.length - 1;
    }
    const part1 = amtStr.slice(0, sliceIndex);
    const part2 = amtStr.slice(sliceIndex);
    const withCommas = part1.toString().replace(/\B(?=(\d{2})+(?!\d))/g, ',');
    return `${withCommas}${part2}`;
  }
  formatAmount = value => {
    return `Rs. ${this.formatToNepaliStyle(value)}`;
  };
  renderItem = ({ item }) => (
    <View style={{ marginBottom: 20 }}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() =>
          this.props.navigation.navigate('DetailScreen', {
            slug: item.slug_url,
          })
        }
      >
        <Image
          style={{
            height: 300,
            width: '100%',
            backgroundColor: 'white',
            borderRadius: 10,
            marginTop: 20,
            shadowOpacity: 3,
            shadowRadius: 4,
            marginBottom: 20,
          }}
          source={
            item.media && item.media.images.length > 0
              ? {
                uri: `${IMAGE_URL}${item.media.images[0].id.path}`,
              }
              : tempImg3
          }
        />
      </TouchableOpacity>
      <View>
        <View
          style={{
            marginHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <Text style={{ fontSize: 12, color: 'gray' }}>
            {item.address.area_id && item.address.area_id.name
              ? item.address.area_id.name
              : 'loading'}
            {', '}
            {item.address.city_id && item.address.city_id.name
              ? item.address.city_id.name
              : 'loading'}
          </Text>
          <Text style={{ fontSize: 20, color: 'blue' }}>
            {item.price && item.price.value
              ? this.formatAmount(item.price.value)
              : 'loading'}
          </Text>
        </View>
        <View
          style={{
            marginHorizontal: 20,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <Text>
            {item.location_property && item.location_property.total_area
              ? item.location_property.total_area
              : 'loading'}{' '}
            {item.location_property.total_area_unit &&
              item.location_property.total_area_unit.title
              ? item.location_property.total_area_unit.title
              : 'loading'}
          </Text>
          <Text style={{ fontSize: 12, color: 'gray' }}>
            {item.is_negotiable === true ? (
              <Text>Price: Negotiable</Text>
            ) : (
                <Text>Price: Non-Negotiable</Text>
              )}
          </Text>
        </View>
      </View>
    </View>
  );
  render() {
    // const { selectFilterData } = this.props;
    return (
      <SafeAreaView>
        <ScrollView>
          <View style={{ marginHorizontal: 20, marginTop: 40 }}>
            <View style={{ flexDirection: 'row', marginHorizontal: 10 }}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Explore')}
              >
                <Image
                  style={{ height: 25, width: 25, marginTop: 20 }}
                  source={require('../../../assets/left_blue.png')}
                />
              </TouchableOpacity>
              <Text
                style={{
                  marginTop: 20,
                  marginLeft: 20,
                  color: '#3F51B5',
                  fontWeight: 'bold',
                  fontSize: 15,
                }}
              >
                HOUSES
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                marginTop: 10,
              }}
            >
              <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
                <TouchableOpacity
                  onPress={() => this.setState({ modalVisible: true })}
                >
                  <Image
                    style={{ height: 25, width: 25, marginTop: 20 }}
                    source={require('../../../assets/sort.png')}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    marginTop: 20,
                    marginLeft: 10,
                    color: '#3F51B5',
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}
                >
                  Filter
                </Text>
              </View>
              <FilterModal
                modalVisible={this.state.modalVisible}
                setModalVisible={this.setModalVisible}
                navigate={this.props.navigation.navigate}
              />

              <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('Explore')}
                >
                  <Image
                    style={{ height: 25, width: 25, marginTop: 20 }}
                    source={require('../../../assets/sort.png')}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    marginTop: 20,
                    marginLeft: 10,
                    color: '#3F51B5',
                    fontWeight: 'bold',
                    fontSize: 15,
                  }}
                >
                  Sort By
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'column', marginHorizontal: 20 }}>
              <View
                style={{
                  width: 0,
                  height: 0,
                  marginLeft: 30,
                  borderLeftWidth: 10,
                  borderRightWidth: 10,
                  borderBottomWidth: 16,
                  borderStyle: 'solid',
                  backgroundColor: 'transparent',
                  borderLeftColor: '#fff',
                  borderRightColor: '#fff',
                  borderBottomColor: '#F0F0F0',
                }}
              />
              <View
                style={{
                  height: 50,
                  width: '100%',
                  backgroundColor: '#F0F0F0',
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Text style={{ color: '#8A8A8F' }}>
                  Use filter to narrow down search results.
                </Text>
              </View>
            </View>
            <FlatList
              data={this.props.selectFilterData}
              renderItem={this.renderItem}
              keyExtractor={item => item._id}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  query: setQueryData,
  selectFilterData,
});
const mapDispatchToProps = {
  setFilterDataValue,
  filterPropertyData,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
