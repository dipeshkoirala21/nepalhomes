/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import SkeletonContent from 'react-native-skeleton-content';

class skeleton extends Component {
  render() {
    return (
      <SkeletonContent
        containerStyle={{
          height: 350,
          width: 300,
          flex: 1,
          backgroundColor: '#FFFFFF',
          flexDirection: 'column',
          marginHorizontal: 10,
          borderRadius: 5,
          borderColor: '#fff',
          elevation: 4,
          borderWidth: 1,
          alignItems: 'center',
          position: 'relative',
          marginBottom: 20,
        }}
        animationDirection="horizontalRight"
        highlightColor="#d3d3d3"
        layout={[
          // long line
          { width: 300, height: 300, marginBottom: 5 },
          // short line
          { width: 300, height: 15, marginBottom: 5 },
          { width: 250, height: 10, marginBottom: 5 },
          { width: 200, height: 10, marginBottom: 5 },
        ]}
        isLoading={true}
      />
    );
  }
}

export default skeleton;
