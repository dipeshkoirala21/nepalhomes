/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, Image, FlatList, TouchableOpacity, ScrollView } from 'react-native';
import { selectProjectData, selectDataLoading } from '../../redux/property/property.selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { projectPropertyData } from '../../redux/property/property.actions';
import tempImg3 from '../../../assets/home.png';
import { IMAGE_URL } from '../../api';
import MyLoader from '../component/propertyskeleton';

export class ProjectProperty extends Component {
  state = {
    project: [],
  };
  async componentDidMount() {
    const item = await this.props.projectPropertyData();
    // console.log(item.data.data.properties);

    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({
      project: item.data.data,
    });
  }
  formatToNepaliStyle(amt) {
    if (!amt) {
      return '0';
    }
    const amtStr = `${amt}`;
    const indexOfPeriod = amtStr.indexOf('.');
    let sliceIndex = indexOfPeriod - 1;
    if (indexOfPeriod === -1) {
      sliceIndex = amtStr.length - 1;
    }
    const part1 = amtStr.slice(0, sliceIndex);
    const part2 = amtStr.slice(sliceIndex);
    const withCommas = part1.toString().replace(/\B(?=(\d{2})+(?!\d))/g, ',');
    return `${withCommas}${part2}`;
  }
  formatAmount = value => {
    return `Rs. ${this.formatToNepaliStyle(value)}`;
  };
  renderItem = ({ item }) => {
    // console.log(item._id, "hello")
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#FFFFFF',
          flexDirection: 'column',
          marginHorizontal: 10,
          borderRadius: 5,
          borderColor: '#fff',
          elevation: 4,
          borderWidth: 1,
          alignItems: 'center',
          position: 'relative',
          marginBottom: 20,
          width: 300,
        }}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() =>
            this.props.navigate('TypeDetails', {
              slug: item.slug_url,
              id: item._id,
            })
          }
        >
          <Image
            style={{
              height: 280,
              width: 300,
              borderTopWidth: 0.5,
              borderColor: '#fff',
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
            }}
            source={
              item && item.media.images.length > 0
                ? {
                  uri: `${IMAGE_URL}${item.media.images[0].id.path}`,
                }
                : tempImg3
            }
          />

          <View>
            <View style={{ width: '100%' }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 18,
                  color: '#000',
                  marginTop: 5,
                  marginLeft: 10,
                }}
                numberOfLines={1}
              >
                {item && item.basic.title
                  ? item.basic.title
                  : 'loading'}
              </Text>
              <Text
                style={{
                  fontWeight: 'normal',
                  fontSize: 15,
                  color: '#8A8A8F',
                  marginTop: 5,
                  marginLeft: 10,
                }}
              >
                {item && item.address.area_id.name
                  ? item.address.area_id.name
                  : 'loading'}
                {', '}
                {item && item.address.city_id.name
                  ? item.address.city_id.name
                  : 'loading'}
              </Text>
              <Text
                style={{
                  fontWeight: 'normal',
                  fontSize: 18,
                  color: '#202B8B',
                  marginTop: 5,
                  marginLeft: 10,
                }}
              >
                {item.price && item.price.value
                  ? this.formatAmount(item.price.value)
                  : 'loading'}
              </Text>
            </View>
            {/* <View
              style={{
                width: '20%',
                flexDirection: 'row',
                justifyContent: 'flex-end',
                marginTop: 20,
              }}
            >
              <View
                style={{
                  width: 0,
                  height: 0,
                  borderLeftWidth: 20,
                  borderRightWidth: 0,
                  borderBottomWidth: 25,
                  borderStyle: 'solid',
                  backgroundColor: 'transparent',
                  borderLeftColor: '#fff',
                  borderRightColor: '#fff',
                  borderBottomColor: '#202B8B',
                  bottom: 0,
                  marginTop: 5,
                }}
              />
              <View
                style={{
                  height: 30,
                  width: 50,
                  borderTopLeftRadius: 8,
                  backgroundColor: '#202B8B',
                  justifyContent: 'center',
                  alignItems: 'center',
                  bottom: 0,
                  borderBottomRightRadius: 5,
                }}
              >
                <Text style={{ color: '#fff' }}>20 Cr</Text>
              </View>
            </View> */}
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    // const { loading } = this.props;
    return (
      <View>
        {/* <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <View
            style={{ justifyContent: 'space-around', flexDirection: 'row' }}
          >
            {loading && <MyLoader />}
            {loading && <MyLoader />}
            {loading && <MyLoader />}
            {loading && <MyLoader />}
            {loading && <MyLoader />}
          </View>
        </ScrollView> */}
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.project}
          renderItem={this.renderItem}
          keyExtractor={item => item._id}
        />
      </View>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  projectdata: selectProjectData,
  loading: selectDataLoading,
});
const mapDispatchToProps = {
  projectPropertyData,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectProperty);
