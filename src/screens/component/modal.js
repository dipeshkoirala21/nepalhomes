/* eslint-disable prettier/prettier */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import {
  Text,
  View,
  Modal,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Picker,
  StyleSheet,
} from 'react-native';
import RNPicker from 'rn-modal-picker';
import CheckBox from 'react-native-check-box';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { enumsData } from '../../redux/enums/enums.actions';
import { selectEnumsData } from '../../redux/enums/enums.selectors';
import { selectFilterData } from '../../redux/property/property.selectors';
import { filterPropertyData } from '../../redux/property/property.actions';
import { setQueryData } from '../../redux/property/property.selectors';
import { setFilterDataValue } from '../../redux/property/property.actions';
import { ScrollView } from 'react-native-gesture-handler';
import { selectLocationData } from '../../redux/location/location.selectors';
import { locationData } from '../../redux/location/location.actions';

class FilterModal extends Component {
  state = {
    query: {},
    moreOption: false,
    placeHolderText: 'SEARCH LOCATION...',
    selectedText: '',
  };
  async componentDidMount() {
    try {
      await this.props.enumsData();
      await this.props.locationData();
    } catch (error) {
      console.log(error);
    }
  }

  setModalVisible = value => {
    this.props.setModalVisible(value);
  };
  toggleOption = value => {
    this.setState({ moreOption: value });
  };
  // handleChange = name => text => {
  //   this.setState({ [name]: text });
  //   this.setState({ query: { key: `find_${name}`, value: text } });
  // };
  handleSearchPage = async () => {
    const { query } = this.props;
    let queryStr = '';
    Object.keys(query).forEach(item => {
      if (query[item] !== null) {
        queryStr = `${queryStr}&${item}=${query[item]}`;
      }
    });
    const data = await this.props.filterPropertyData(queryStr);
    // console.log(data.success);
    if (data.success === true) {
      this.setModalVisible(false);
      //alert(`total data: ${data.totaldata}`);
      this.props.navigate('SearchScreen');
      // {
      //   filteredData: data.data,
      // });
    } else {
      ToastAndroid.show(
        `Something Went Wrong! ${JSON.stringify(data)}`,
        ToastAndroid.SHORT,
      );
    }
  };

  // sendDataRequest({ is_premium: this.state.is_premium });
  onCheckBoxSelected = (name, value) => {
    this.props.setFilterDataValue({
      key: name,
      value: value === false ? '' : true,
    });
  };

  onPurposeSelected = id => {
    this.props.setFilterDataValue({
      key: 'find_property_purpose',
      value: id,
    });
  };
  onCategorySelected = id => {
    this.props.setFilterDataValue({
      key: 'find_property_category',
      value: id,
    });
  };
  onLocationSelected = () => {
    this.props.setFilterDataValue({
      // key: 'find_is_negotiable',
      // value:id,
    });
  };
  onPriceRangeSelected = (key, value) => {
    this.props.setFilterDataValue({
      key,
      value,
    });
  };
  onPropertyFaceSelected = id => {
    this.props.setFilterDataValue({
      key: 'find_property_face',
      value: id,
    });
  };
  _selectedValue(index, item) {
    console.log(item);
    // this.setState({ selectedText: item.name });
    this.handleLocationChange(item);
  }
  handleLocationChange = event => {
    this.setState({ key: 'find_location', value: event });
    this.setState({ selectedText: event.name });
    if (this.props.query.find_state_id) {
      this.props.setFilterDataValue({ key: 'find_state_id', value: '' });
    }
    if (this.props.query.find_district_id) {
      this.props.setFilterDataValue({ key: 'find_district_id', value: '' });
    }
    if (this.props.query.find_vdc_id) {
      this.props.setFilterDataValue({ key: 'find_vdc_id', value: '' });
    }
    if (this.props.query.find_area_id) {
      this.props.setFilterDataValue({ key: 'find_area_id', value: '' });
    }
    if (event.custom === 'State') {
      this.props.setFilterDataValue({ key: 'find_state_id', value: event.id });
    }
    if (event.custom === 'District') {
      this.props.setFilterDataValue({ key: 'find_district_id', value: event.id });
    }
    if (event.custom === 'Vdc') {
      this.props.setFilterDataValue({ key: 'find_vdc_id', value: event.id });
    }
    if (event.custom === 'Area') {
      this.props.setFilterDataValue({ key: 'find_area_id', value: event.id });
    }
  };
  render() {
    console.log(this.state.selectedText);
    const { selectEnumsData, query, locations } = this.props;
    const optionStates = locations.allState
      ? locations.allState.map(function state(each) {
        let displayName = '';
        displayName = each.name.replace(/-/g, ', ');
        return {
          id: each._id,
          name: displayName,
          custom: 'State',
        };
      })
      : [];
    const optionDistricts = locations.allDistrict
      ? locations.allDistrict.map(function district(each) {
        const displayName1 = each.slug.replace('-', ' ');
        const displayName2 = displayName1.replace(/-/g, ', ');
        return {
          id: each._id,
          name: displayName2,
          custom: 'District',
        };
      })
      : [];
    const optionVdc = locations.allVdc
      ? locations.allVdc.map(function vdc(each) {
        const displayName1 = each.slug.replace('-', ' ');
        const displayName2 = displayName1.replace(/-/g, ', ');
        return {
          id: each._id,
          name: displayName2,
          custom: 'Vdc',
        };
      })
      : [];
    const optionArea = locations.allArea
      ? locations.allArea.map(function state(each) {
        const displayName1 = each.slug.replace('-', ' ');
        const displayName2 = displayName1
          .split('-')
          .reverse()
          .join(', ');
        // const displayName2 = displayName1.replace(/-/g, ', ');
        return {
          id: each._id,
          name: displayName2,
          custom: 'Area',
        };
      })
      : [];
    const optionLocations = optionStates.concat(
      optionArea,
      optionVdc,
      optionDistricts,
    );
    const optionMaxPrice = [
      { id: '1', name: 'Up To 50 K' },
      { id: '2', name: '50 K to 5 Lakh' },
      { id: '3', name: '5 Lakh to 50 Lakh' },
      { id: '4', name: '50 Lakh to 3 Cr.' },
      { id: '5', name: '3 Cr. to max' },
    ];
    return (
      <View>
        {Object.keys(selectEnumsData).length > 0 && (
          <Modal
            animationType="fade"
            handleClick={this.setModalVisible}
            transparent={true}
            visible={this.props.modalVisible}
            onRequestClose={() => {
              this.setModalVisible(false);
            }}
          >
            <ScrollView>
              <View
                style={{
                  backgroundColor: '#fff',
                  height: this.state.moreOption === true ? 660 : 460,
                  marginTop: 20,
                  marginHorizontal: 10,
                  borderWidth: 1,
                  borderColor: '#fff',
                  borderRadius: 5,
                  elevation: 8,
                  marginBottom: 20,
                }}
              >
                <View style={{ flexDirection: 'row', marginHorizontal: 20 }}>
                  <TouchableOpacity onPress={() => this.setModalVisible(false)}>
                    <Image
                      style={{ height: 25, width: 25, marginTop: 20 }}
                      source={require('../../../assets/back.png')}
                    />
                  </TouchableOpacity>
                  {/* <TextInput
                  style={{
                    width: '100%',
                    color: '#4A4A4A',
                    marginTop: 20,
                    marginLeft: 20,
                  }}
                  value={this.state.location}
                  onChangeText={this.handleChange('location')}
                  placeholder={'SEARCH BASED ON LOCATION...'}
                /> */}
                  <View style={Styles.container}>
                    <RNPicker
                      dataSource={optionLocations}
                      dummyDataSource={optionLocations}
                      defaultValue={false}
                      pickerTitle={'LOCATION'}
                      showSearchBar={true}
                      disablePicker={false}
                      changeAnimation={'none'}
                      searchBarPlaceHolder={'Search.....'}
                      showPickerTitle={true}
                      searchBarContainerStyle={this.props.searchBarContainerStyle}
                      pickerStyle={Styles.pickerStyle}
                      pickerItemTextStyle={Styles.listTextViewStyle}
                      selectedLabel={this.state.selectedText}
                      placeHolderLabel={this.state.placeHolderText}
                      selectLabelTextStyle={Styles.selectLabelTextStyle}
                      placeHolderTextStyle={Styles.placeHolderTextStyle}
                      dropDownImageStyle={Styles.dropDownImageStyle}
                      dropDownImage={require('../../../assets/dropdown.png')}
                      selectedValue={(index, item) => this._selectedValue(index, item)}
                    />
                  </View>
                </View>
                <View
                  style={{
                    marginTop: 20,
                    marginHorizontal: 20,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                  }}
                >
                  <CheckBox
                    style={{ flex: 1, padding: 10 }}
                    onClick={() =>
                      this.onCheckBoxSelected(
                        'find_is_negotiable',
                        !query.find_is_negotiable,
                      )
                    }
                    isChecked={
                      query.find_is_negotiable.length <= 0
                        ? false
                        : query.find_is_negotiable
                    }
                    rightText={'Negotiable'}
                  />
                  <CheckBox
                    style={{ flex: 1, padding: 10 }}
                    onClick={() =>
                      this.onCheckBoxSelected(
                        'find_is_premium',
                        !query.find_is_premium,
                      )
                    }
                    isChecked={
                      query.find_is_premium.length <= 0
                        ? false
                        : query.find_is_premium
                    }
                    rightText={'Premium'}
                  />
                </View>
                <View
                  style={{
                    marginHorizontal: 20,
                  }}
                >
                  <CheckBox
                    style={{ flex: 1, padding: 10 }}
                    onClick={() =>
                      this.onCheckBoxSelected(
                        'find_is_featured',
                        !query.find_is_featured,
                      )
                    }
                    isChecked={
                      query.find_is_featured.length <= 0
                        ? false
                        : query.find_is_featured
                    }
                    rightText={'Featured'}
                  />
                </View>
                <View style={{ marginTop: 20, marginHorizontal: 20 }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: 'normal',
                      color: '#1C3D5A',
                      marginTop: 10,
                    }}
                  >
                    Purpose
                </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    marginTop: 10,
                    flexWrap: 'wrap',
                    marginHorizontal: 20,
                  }}
                >
                  {selectEnumsData.property_purpose ? (
                    selectEnumsData.property_purpose.map(each => (
                      <TouchableOpacity
                        key={each._id}
                        onPressIn={() => this.onPurposeSelected(each._id)}
                        style={{
                          height: 30,
                          padding: 10,
                          borderWidth: 1,
                          borderRadius: 5,
                          backgroundColor: '#F4F4F7',
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderColor:
                            query.find_property_purpose === each._id
                              ? '#0291DD'
                              : '#202B8B',
                        }}
                      >
                        <Text style={{ color: '#202B8B' }}>{each.title}</Text>
                      </TouchableOpacity>
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </View>
                <View style={{ marginTop: 20, marginHorizontal: 20 }}>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: 'normal',
                      color: '#1C3D5A',
                      marginTop: 10,
                    }}
                  >
                    Category
                </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 10,
                    flexWrap: 'wrap',
                    marginHorizontal: 20,
                  }}
                >
                  {selectEnumsData.property_category ? (
                    selectEnumsData.property_category.map(each => (
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginRight: 10,
                          marginBottom: 10,
                        }}
                        key={each._id}
                      >
                        <TouchableOpacity
                          style={{
                            height: 30,
                            padding: 10,
                            borderWidth: 1,
                            borderRadius: 5,
                            backgroundColor: '#F4F4F7',
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderColor:
                              query.find_property_category === each._id
                                ? '#0291DD'
                                : '#202B8B',
                          }}
                          onPress={() => this.onCategorySelected(each._id)}
                        >
                          <Text style={{ color: '#202B8B' }}>{each.title}</Text>
                        </TouchableOpacity>
                      </View>
                    ))
                  ) : (
                      <Text>loading...</Text>
                    )}
                </View>
                {this.state.moreOption === true ? (
                  <View>
                    <View>
                      <View style={{ marginTop: 10, marginHorizontal: 20 }}>
                        <Text
                          style={{
                            fontSize: 14,
                            fontWeight: 'normal',
                            color: '#1C3D5A',
                            marginTop: 10,
                          }}
                        >
                          Price Range
                      </Text>
                      </View>
                      <View
                        style={{
                          borderRadius: 5,
                          borderColor: '#979797',
                          borderWidth: 1,
                          marginHorizontal: 20,
                          marginTop: 10,
                        }}
                      >
                        <Picker
                          selectedValue={query.find_selected_price}
                          style={{
                            width: '100%',
                            height: 50,
                          }}
                          onValueChange={(itemValue, itemIndex) =>
                            this.onPriceRangeSelected('find_selected_price', itemValue)
                          }
                        >
                          {optionMaxPrice ? (
                            optionMaxPrice.map(each => (
                              <Picker.Item
                                label={each.name}
                                value={each.id}
                                key={each.id}
                              />
                            ))
                          ) : (
                              <Text>loading...</Text>
                            )}
                        </Picker>
                      </View>
                    </View>
                    <View style={{ marginTop: 10, marginHorizontal: 20 }}>
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: 'normal',
                          color: '#1C3D5A',
                          marginTop: 10,
                        }}
                      >
                        Property Face
                    </Text>
                    </View>
                    <View
                      style={{
                        borderRadius: 5,
                        borderColor: '#979797',
                        borderWidth: 1,
                        marginHorizontal: 20,
                        marginTop: 10,
                      }}
                    >
                      <Picker
                        selectedValue={query.find_property_face}
                        style={{
                          width: '100%',
                          height: 50,
                        }}
                        onValueChange={(itemValue, itemIndex) =>
                          this.onPropertyFaceSelected(itemValue)
                        }
                      >
                        {selectEnumsData.property_face ? (
                          selectEnumsData.property_face.map(each => (
                            <Picker.Item
                              label={each.title}
                              value={each._id}
                              key={each._id}
                            />
                          ))
                        ) : (
                            <Text>loading...</Text>
                          )}
                      </Picker>
                    </View>
                  </View>
                ) : null}
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}
                >
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginTop: 20,
                      marginHorizontal: 20,
                    }}
                  >
                    {this.state.moreOption === false ? (
                      <TouchableOpacity
                        style={{
                          height: 30,
                          padding: 10,
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderColor: '#202B8B',
                        }}
                        onPress={() => this.toggleOption(true)}
                      >
                        <Text style={{ color: '#202B8B', fontSize: 15 }}>
                          more options...
                      </Text>
                      </TouchableOpacity>
                    ) : (
                        <TouchableOpacity
                          style={{
                            height: 30,
                            padding: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderColor: '#202B8B',
                          }}
                          onPress={() => this.toggleOption(false)}
                        >
                          <Text style={{ color: '#202B8B', fontSize: 15 }}>
                            less options...
                      </Text>
                        </TouchableOpacity>
                      )}
                  </View>
                  <View
                    style={{
                      marginHorizontal: 20,
                      justifyContent: 'flex-end',
                      flexDirection: 'row',
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        marginTop: 20,
                        width: 100,
                        height: 40,
                        backgroundColor: '#202B8B',
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderColor: '#006395',
                        borderWidth: 1,
                        borderRadius: 4,
                      }}
                      onPress={this.handleSearchPage}
                    >
                      <Text
                        style={{
                          color: 'white',
                          fontSize: 15,
                          fontWeight: 'bold',
                        }}
                      >
                        Search
                    </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
            <TouchableOpacity
              onPress={() => this.setModalVisible(false)}
              style={{ flex: 1 }}
            />
          </Modal>
        )}
      </View>
    );
  }
}
const Styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginLeft: 10,
  },

  searchBarContainerStyle: {
    marginBottom: 10,
    flexDirection: 'row',
    height: 40,
    shadowOpacity: 1.0,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    backgroundColor: 'rgba(255,255,255,1)',
    shadowColor: '#d3d3d3',
    borderRadius: 10,
    elevation: 3,
    marginLeft: 10,
    marginRight: 10,
  },

  selectLabelTextStyle: {
    color: '#000',
    textAlign: 'left',
    width: '90%',
    padding: 10,
    flexDirection: 'row',
  },
  placeHolderTextStyle: {
    color: '#D3D3D3',
    padding: 10,
    textAlign: 'left',
    width: '90%',
    flexDirection: 'row',
  },
  dropDownImageStyle: {
    marginLeft: 10,
    width: 10,
    height: 10,
    alignSelf: 'center',
  },
  listTextViewStyle: {
    color: '#000',
    marginVertical: 10,
    flex: 0.9,
    marginLeft: 20,
    marginHorizontal: 10,
    textAlign: 'left',
  },
  pickerStyle: {
    marginLeft: 18,
    elevation: 3,
    paddingRight: 25,
    marginRight: 10,
    marginBottom: 2,
    shadowOpacity: 1.0,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    borderWidth: 1,
    shadowRadius: 10,
    backgroundColor: 'rgba(255,255,255,1)',
    shadowColor: '#d3d3d3',
    borderRadius: 5,
    flexDirection: 'row',
  },
});
const mapStateToProps = createStructuredSelector({
  selectEnumsData,
  query: setQueryData,
  selectFilterData,
  locations: selectLocationData,
});
const mapDispatchToProps = {
  enumsData,
  setFilterDataValue,
  filterPropertyData,
  locationData,
};

export default connect(mapStateToProps, mapDispatchToProps)(FilterModal);
