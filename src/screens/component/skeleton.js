/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import SkeletonContent from 'react-native-skeleton-content';

class skeleton extends Component {
  render() {
    return (
      <SkeletonContent
        containerStyle={{
          height: 400,
          width: '100%',
          backgroundColor: '#fff',
          borderRadius: 5,
          borderColor: '#000',
          marginTop: 20,
          elevation: 4,
          bottom: 10,
        }}
        animationDirection="horizontalRight"
        highlightColor="#d3d3d3"
        layout={[
          // long line
          { width: 390, height: 350, marginBottom: 6 },
          // short line
          { width: 390, height: 20, marginBottom: 6 },
          { width: 180, height: 15, marginBottom: 6 },
        ]}
        isLoading={true}
      />
    );
  }
}

export default skeleton;
