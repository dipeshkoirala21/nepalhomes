/* eslint-disable eslint-comments/no-unused-disable */
/* eslint-disable react-native/no-inline-styles */
import React, { Component } from 'react';
import { Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import {
  selectHotData,
  selectDataLoading,
} from '../../redux/property/property.selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { hotpropertyData } from '../../redux/property/property.actions';
import tempImg3 from '../../../assets/home.png';
import { IMAGE_URL } from '../../api';
import MyLoader from '../component/propertyskeleton';
import { ScrollView } from 'react-native-gesture-handler';

export class HotProperty extends Component {
  state = {
    hot: [],
  };
  async componentDidMount() {
    const item = await this.props.hotpropertyData();
    // console.log(item.data.data.properties);

    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({
      hot: item.data.data.properties,
    });
  }
  formatToNepaliStyle(amt) {
    if (!amt) {
      return '0';
    }
    const amtStr = `${amt}`;
    const indexOfPeriod = amtStr.indexOf('.');
    let sliceIndex = indexOfPeriod - 1;
    if (indexOfPeriod === -1) {
      sliceIndex = amtStr.length - 1;
    }
    const part1 = amtStr.slice(0, sliceIndex);
    const part2 = amtStr.slice(sliceIndex);
    const withCommas = part1.toString().replace(/\B(?=(\d{2})+(?!\d))/g, ',');
    return `${withCommas}${part2}`;
  }
  formatAmount = value => {
    return `Rs. ${this.formatToNepaliStyle(value)}`;
  };
  renderItem = ({ item }) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#FFFFFF',
          flexDirection: 'column',
          marginHorizontal: 10,
          borderRadius: 5,
          borderColor: '#fff',
          elevation: 4,
          borderWidth: 1,
          alignItems: 'center',
          position: 'relative',
          marginBottom: 20,
          width: 300,
        }}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() =>
            this.props.navigate('TypeDetails', {
              slug: item.id.slug_url,
            })
          }
        >
          <Image
            style={{
              height: 280,
              width: 300,
              borderTopWidth: 0.5,
              borderColor: '#fff',
              borderTopLeftRadius: 5,
              borderTopRightRadius: 5,
            }}
            source={
              item.id && item.id.media.images.length > 0
                ? {
                    uri: `${IMAGE_URL}${item.id.media.images[0].id.path}`,
                  }
                : tempImg3
            }
          />
          {/* item1.properties[0].media.images[0].path */}

          <View>
            <View style={{ width: '100%' }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 18,
                  color: '#000',
                  marginTop: 5,
                  marginLeft: 10,
                }}
                numberOfLines={1}
              >
                {item.id && item.id.basic.title
                  ? item.id.basic.title
                  : 'loading'}
              </Text>
              <Text
                style={{
                  fontWeight: 'normal',
                  fontSize: 15,
                  color: '#8A8A8F',
                  marginTop: 5,
                  marginLeft: 10,
                }}
              >
                {item.id && item.id.address.area_id.name
                  ? item.id.address.area_id.name
                  : 'loading'}
                {', '}
                {item.id && item.id.address.city_id.name
                  ? item.id.address.city_id.name
                  : 'loading'}
              </Text>
              <Text
                style={{
                  fontWeight: 'normal',
                  fontSize: 18,
                  color: '#202B8B',
                  marginTop: 5,
                  marginLeft: 10,
                }}
              >
                {item.id && item.id.price.value
                  ? this.formatAmount(item.id.price.value)
                  : 'loading'}
              </Text>
              {/* {console.log(item.id.price.value)} */}
            </View>
            {/* <View
              style={{
                width: '20%',
                flexDirection: 'row',
                justifyContent: 'flex-end',
                marginTop: 20,
              }}
            >
              <View
                style={{
                  width: 0,
                  height: 0,
                  borderLeftWidth: 20,
                  borderRightWidth: 0,
                  borderBottomWidth: 25,
                  borderStyle: 'solid',
                  backgroundColor: 'transparent',
                  borderLeftColor: '#fff',
                  borderRightColor: '#fff',
                  borderBottomColor: '#D3D3D3',
                  bottom: 0,
                  marginTop: 5,
                }}
              />
              <View
                // eslint-disable-next-line react-native/no-inline-styles
                style={{
                  height: 30,
                  width: 50,
                  borderTopLeftRadius: 8,
                  backgroundColor: '#D3D3D3',
                  justifyContent: 'center',
                  alignItems: 'center',
                  bottom: 0,
                  borderBottomRightRadius: 5,
                }}
              >
                <Text style={{ color: '#8A8A8F' }}>20 Cr</Text>
              </View>
            </View> */}
          </View>
        </TouchableOpacity>
      </View>
    );
  };
  render() {
    // const { loading } = this.props;
    return (
      <View>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.hot}
          renderItem={this.renderItem}
          keyExtractor={item => item._id}
        />
      </View>
    );
  }
}
const mapStateToProps = createStructuredSelector({
  hotdata: selectHotData,
  loading: selectDataLoading,
});
const mapDispatchToProps = {
  hotpropertyData,
};

export default connect(mapStateToProps, mapDispatchToProps)(HotProperty);
