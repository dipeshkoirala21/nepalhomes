import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { forgotPasswordRequest } from '../../redux/auth/auth.actions';

class ForgotPassword extends Component {
  state = {
    email: '',
  }
  handleSubmit = () => {
    this.props.forgotPasswordRequest(this.state);
    ToastAndroid.show("Password for " + this.state.email + " has been sent to email", ToastAndroid.SHORT);

  };
  handleChange = name => text => this.setState({ [name]: text });
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', marginHorizontal: 40, marginTop: 40 }}>
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold' }}>Forgot Password ?</Text>
        <Text style={{ color: 'black', fontSize: 15 }}>Don’t worry! Just fill in your email and we’ll help you reset your password.</Text>
        <TextInput
          style={{
            height: 40,
            width: '80%',
            borderBottomWidth: 1,
            borderBottomColor: '#d3d3d3',
            marginBottom: 5,
            marginTop: 80,
            padding: 10,
          }}
          value={this.state.email}
          onChangeText={this.handleChange('email')}
          placeholder={'Email'}
        />
        <TouchableOpacity
          style={{
            marginTop: 20,
            width: 300,
            height: 40,
            backgroundColor: '#006395',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: '#006395',
            borderWidth: 1,
            borderRadius: 4,
          }}
          onPress={this.handleSubmit}
        >
          <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
            Submit
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            marginTop: 20,
            width: 300,
            height: 40,
            backgroundColor: '#006395',
            alignItems: 'center',
            justifyContent: 'center',
            borderColor: '#006395',
            borderWidth: 1,
            borderRadius: 4,
          }}
          onPress={() => this.props.navigation.navigate("Login")}
        >
          <Text style={{ color: 'white', fontSize: 15, fontWeight: 'bold' }}>
            Back
          </Text>
        </TouchableOpacity>
      </View>
    )
  }
}
const mapStateToProps = null;

const mapDispatchToProps = { forgotPasswordRequest };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ForgotPassword);