import produce from 'immer';
import * as types from './property.types';

const INITIAL_STATE = {
  loading: false,
  recentdata: {},
  hotdata: {},
  trendingdata: {},
  filterdata: {},
  detailData: {},
  projectData: {},
  favourite: {},
  postProperty: {
    basic: {
      title: '',
      description: '',
      property_purpose: '', // For Sale, Rent, Lease
      property_type: '', // Residential, Plots, Commercial
      property_category: '', // House, Land, Flat, Apartment, Business, Office Space , Hostel
    },
    address: {
      state_id: '5dc016660c91b016a0610d68', // enum
      district_id: '5dc01a28ccd2df43948cf7a4', // enum
      city_id: '5dca424457df991450d2aaab', // enum
      area_id: '5dca438a57df991450d2aac4', // enum
      house_no: '',
    },
    location_property: {
      total_area_unit: '5d6797781881bf21a423ec30', // enum
      total_area: '',
      built_area: '',
      built_area_unit: '5d6797781881bf21a423ec30', // enum
      property_face: '5d675c2ca9c9a02f4c40385c', // enum
      road_access_value: '',
      road_access_length_unit: '5d6e51d8b943ac1d3c456862', // enum
      road_access_road_type: '5d68ae7d3b84b212d47693e9', // enum
    },
    building: {
      built_year: 0,
      built_month: '',
      calender_type: '5d6cc52873552113c0396023', // AD,BS
      total_floor: 1,
      furnishing: '5d6e2f142499501fe8fa92fd', // Full,Semi,Un
      no_of: {
        kitchen: 0,
        dinningroom: 1,
        bedroom: 0,
        bathroom: 0,
        hall: 1,
      },
      parking: '',
      amenities: [], // enum
    },
    media: {
      images: [],
      youtube_video_id: 'abUslZTOJmE',
    },
    price: {
      value: '',
      currency: '5d6e185db75d182a28789cb8', // enum
      label: '5d6ce42373552113c039602f', // Per Ana, Per SqFeet
    },
    agency_id: '',
    is_active: false,
    is_featured: false,
    is_premium: false,
    is_negotiable: false,
    is_verified: false,
    tags: [],
  },
  query: {
    find_property_category: null,
    find_property_type: null,
    find_selected_price: null,
    find_property_face: null,
    find_road_access_road_type: null,
    find_is_negotiable: '',
    find_is_premium: '',
    find_is_featured: '',
    find_property_purpose: '',
  },
};
const reducer = (state = INITIAL_STATE, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.RECENT_PROPERTY_DATA_REQUEST:
        draft.loading = true;
        break;
      case types.RECENT_PROPERTY_DATA_SUCCESS:
        draft.recentdata = action.payload;
        draft.loading = false;
        break;
      case types.RECENT_PROPERTY_DATA_FAILURE:
        draft.loading = false;
        break;
      case types.HOT_PROPERTY_DATA_REQUEST:
        draft.loading = true;
        break;
      case types.HOT_PROPERTY_DATA_SUCCESS:
        draft.hotdata = action.payload;
        draft.loading = false;
        break;
      case types.HOT_PROPERTY_DATA_FAILURE:
        draft.loading = false;
        break;
      case types.TRENDING_PROPERTY_DATA_REQUEST:
        draft.loading = true;
        break;
      case types.TRENDING_PROPERTY_DATA_SUCCESS:
        draft.trendingdata = action.payload;
        draft.loading = false;
        break;
      case types.TRENDING_PROPERTY_DATA_FAILURE:
        draft.loading = false;
        break;
      case types.PROJECT_PROPERTY_DATA_SUCCESS:
        draft.projectData = action.payload;
        break;
      case types.SUCCESS_QUERY_SEARCH_DATA: //get query
        draft.filterdata = action.payload.data;
        break;
      case types.FAILURE_QUERY_SEARCH_DATA: //get query
        draft.filterdata = action.payload;
        break;
      case types.DETAIL_PROPERTY_SUCCESS: //get query
        draft.detailData = action.payload.data;
        break;
      case types.SET_FILTER_DATA: //set state of query
        draft.query[action.payload.key] = action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_SUCCESS:
        draft.postProperty = action.payload;
        break;
      case types.CLEAR_POST_PROPERTY_FIELD:
        draft.postProperty = INITIAL_STATE.postProperty;
        break;
      case types.POST_PROPERTY_SUCCESS:
        draft.postProperty = action.payload;
        break;
      case types.GET_FAVOURITE_PROPERTY_SUCCESS:
      case types.FAVOURITE_PROPERTY_SUCCESS:
        draft.favourite = action.payload;
        break;
      case types.SET_POST_PROPERTY_VALUE_BASIC:
        draft.postProperty.basic[action.payload.key] = action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_PRICE:
        draft.postProperty.price[action.payload.key] = action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_ADDRESS:
        draft.postProperty.address[action.payload.key] = action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_BUILDING:
        draft.postProperty.building[action.payload.key] = action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_NO_OF:
        draft.postProperty.building.no_of[action.payload.key] =
          action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_LOCATION_PROPERTY:
        draft.postProperty.location_property[action.payload.key] =
          action.payload.value;
        break;
      case types.MULTIPLE_PHOTO_SUCCESS:
        draft.postProperty.media.images = [
          ...state.postProperty.media.images,
          ...action.payload.data.map(function images(each) {
            return {
              id: each,
              caption: '5dc3e8d0e1daf24acd5e810d',
            };
          }),
        ];
        break;
      case types.SET_POST_PROPERTY_VALUE_MEDIA:
        draft.postProperty.media[action.payload.key] = action.payload.value;
        break;
      case types.SET_POST_PROPERTY_VALUE_TAGS:
        draft.postProperty.tags = action.payload;
        break;
    }
  });
export default reducer;
