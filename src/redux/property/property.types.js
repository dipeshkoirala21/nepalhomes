export const SET_CURRENT_USER = 'mobile-app/auth/SET_CURRENT_USER';

export const RECENT_PROPERTY_DATA_REQUEST =
  'mobile-app/property/RECENT_PROPERTY_DATA_REQUEST';
export const RECENT_PROPERTY_DATA_SUCCESS =
  'mobile-app/property/RECENT_PROPERTY_DATA_SUCCESS';
export const RECENT_PROPERTY_DATA_FAILURE =
  'mobile-app/property/RECENT_PROPERTY_DATA_FAILURE';

export const HOT_PROPERTY_DATA_REQUEST =
  'mobile-app/property/HOT_PROPERTY_DATA_REQUEST';
export const HOT_PROPERTY_DATA_SUCCESS =
  'mobile-app/property/HOT_PROPERTY_DATA_SUCCESS';
export const HOT_PROPERTY_DATA_FAILURE =
  'mobile-app/property/HOT_PROPERTY_DATA_FAILURE';

export const TRENDING_PROPERTY_DATA_REQUEST =
  'mobile-app/property/TRENDING_PROPERTY_DATA_REQUEST';
export const TRENDING_PROPERTY_DATA_SUCCESS =
  'mobile-app/property/TRENDING_PROPERTY_DATA_SUCCESS';
export const TRENDING_PROPERTY_DATA_FAILURE =
  'mobile-app/property/TRENDING_PROPERTY_DATA_FAILURE';

export const PROJECT_PROPERTY_DATA_REQUEST =
  'mobile-app/property/PROJECT_PROPERTY_DATA_REQUEST';
export const PROJECT_PROPERTY_DATA_SUCCESS =
  'mobile-app/property/PROJECT_PROPERTY_DATA_SUCCESS';
export const PROJECT_PROPERTY_DATA_FAILURE =
  'mobile-app/property/PROJECT_PROPERTY_DATA_FAILURE';

export const REQUEST_QUERY_SEARCH_DATA =
  'mobile-app/property/REQUEST_QUERY_SEARcH_DATA';
export const SUCCESS_QUERY_SEARCH_DATA =
  'mobile-app/property/SUCCESS_QUERY_SEARcH_DATA';
export const FAILURE_QUERY_SEARCH_DATA =
  'mobile-app/property/FAILURE_QUERY_SEARcH_DATA';

export const DETAIL_PROPERTY_REQUEST =
  'mobile-app/property/DETAIL_PROPERTY_REQUEST';
export const DETAIL_PROPERTY_SUCCESS =
  'mobile-app/property/DETAIL_PROPERTY_SUCCESS';
export const DETAL_PROPERTY_FAILURE =
  'mobile-app/property/DETAIL_PROPERTY_FAILURE';

export const SET_FILTER_DATA = 'mobile-app/property/SET_FILTER_DATA';

export const POST_PROPERTY_REQUEST =
  'mobile-app/property/POST_PROPERTY_REQUEST';
export const POST_PROPERTY_SUCCESS =
  'mobile-app/property/POST_PROPERTY_SUCCESS';
export const POST_PROPERTY_FAILURE =
  'mobile-app/property/POST_PROPERTY_FAILURE';

export const CLEAR_POST_PROPERTY_FIELD =
  'mobile-app/property/CLEAR_POST_PROPERTY_FIELD';

export const MULTIPLE_PHOTO_REQUEST = 'mobile-app/property/MULTIPLE_PHOTO_REQUEST';
export const MULTIPLE_PHOTO_SUCCESS = 'mobile-app/property/MULTIPLE_PHOTO_SUCCESS';
export const MULTIPLE_PHOTO_FAILURE = 'mobile-app/property/MULTIPLE_PHOTO_FAILURE';

export const FAVOURITE_PROPERTY_REQUEST = 'mobile-app/property/FAVOURITE_PROPERTY_REQUEST';
export const FAVOURITE_PROPERTY_SUCCESS = 'mobile-app/property/FAVOURITE_PROPERTY_SUCCESS';
export const FAVOURITE_PROPERTY_FAILURE = 'mobile-app/property/FAVOURITE_PROPERTY_FAILURE';

export const GET_FAVOURITE_PROPERTY_REQUEST = 'mobile-app/property/GET_FAVOURITE_PROPERTY_REQUEST';
export const GET_FAVOURITE_PROPERTY_SUCCESS = 'mobile-app/property/GET_FAVOURITE_PROPERTY_SUCCESS';
export const GET_FAVOURITE_PROPERTY_FAILURE = 'mobile-app/property/GET_FAVOURITE_PROPERTY_FAILURE';

export const SET_POST_PROPERTY_VALUE_BASIC =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_BASIC';

export const SET_POST_PROPERTY_VALUE_ADDRESS =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_ADDRESS';

export const SET_POST_PROPERTY_VALUE_LOCATION_PROPERTY =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_LOCATION_PROPERTY';

export const SET_POST_PROPERTY_VALUE_BUILDING =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_BUILDING';

export const SET_POST_PROPERTY_VALUE_NO_OF =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_NO_OF';

export const SET_POST_PROPERTY_VALUE_MEDIA =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_MEDIA';

export const SET_POST_PROPERTY_VALUE_PRICE =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_PRICE';

export const SET_POST_PROPERTY_VALUE_AGENCY_ID =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_AGENCY_ID';

export const SET_POST_PROPERTY_VALUE_TAGS =
  'mobile-app/property/SET_POST_PROPERTY_VALUE_TAGS';
