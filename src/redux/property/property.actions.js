import * as types from './property.types';
import { recentpropertyGet, projectGet, favouriteData, favouriteDataGet } from '../../api';
import { hotPropertyGet, detailProperty } from '../../api';
import { trendingPropertyGet } from '../../api';
import { propertyPost } from '../../api';
import { searchProperty, multiplePhoto } from '../../api';
import { ToastAndroid } from 'react-native';

export const recentpropertyData = () => async dispatch => {
  //parameter ma payload pathaune
  dispatch({ type: types.RECENT_PROPERTY_DATA_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await recentpropertyGet(); //ani yeta line
    //console.log('response', response.data);
    dispatch({
      type: types.RECENT_PROPERTY_DATA_SUCCESS,
      payload: response.data,
    });
    return response;
  } catch (err) {
    dispatch({ type: types.RECENT_PROPERTY_DATA_FAILURE, payload: err });
    ToastAndroid.show('Recent property not retrieved!', ToastAndroid.SHORT);
    throw err;
  }
};

export const hotpropertyData = () => async dispatch => {
  dispatch({ type: types.HOT_PROPERTY_DATA_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await hotPropertyGet();
    // console.log('response', response.data);
    dispatch({
      type: types.HOT_PROPERTY_DATA_SUCCESS,
      payload: response.data,
    });
    return response;
  } catch (err) {
    dispatch({ type: types.HOT_PROPERTY_DATA_FAILURE, payload: err });
    ToastAndroid.show('Hot Property not retrieved!', ToastAndroid.SHORT);
    throw err;
  }
};

export const trendingpropertyData = () => async dispatch => {
  dispatch({ type: types.TRENDING_PROPERTY_DATA_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await trendingPropertyGet();
    //console.log('response', response.data);
    dispatch({
      type: types.TRENDING_PROPERTY_DATA_SUCCESS,
      payload: response.data,
    });
    return response;
  } catch (err) {
    dispatch({ type: types.TRENDING_PROPERTY_DATA_FAILURE, payload: err });
    ToastAndroid.show('Trending Property not retrieved!', ToastAndroid.SHORT);
    throw err;
  }
};
export const projectPropertyData = () => async dispatch => {
  dispatch({ type: types.PROJECT_PROPERTY_DATA_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await projectGet();
    //console.log('response', response.data);
    dispatch({
      type: types.PROJECT_PROPERTY_DATA_SUCCESS,
      payload: response.data,
    });
    return response;
  } catch (err) {
    dispatch({ type: types.PROJECT_PROPERTY_DATA_FAILURE, payload: err });
    ToastAndroid.show('Project Property not retrieved!', ToastAndroid.SHORT);
    throw err;
  }
};
export const filterPropertyData = payload => async dispatch => {
  dispatch({ type: types.REQUEST_QUERY_SEARCH_DATA, payload });
  try {
    //console.log(payload, 'payload');
    const response = await searchProperty(payload);
    // console.log('response', response.data);
    dispatch({
      type: types.SUCCESS_QUERY_SEARCH_DATA,
      payload: response.data,
    });
    return response.data;
  } catch (err) {
    dispatch({ type: types.FAILURE_QUERY_SEARCH_DATA, payload: err });
    ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
    throw err;
  }
};

export const detailPropertyData = payload => async dispatch => {
  dispatch({ type: types.DETAIL_PROPERTY_REQUEST, payload });
  try {
    //console.log(payload, 'payload');
    const response = await detailProperty(payload);
    // console.log('response', response.data);
    dispatch({
      type: types.DETAIL_PROPERTY_SUCCESS,
      payload: response.data,
    });
    return response.data;
  } catch (err) {
    dispatch({ type: types.DETAL_PROPERTY_FAILURE, payload: err });
    ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
    throw err;
  }
};
export const setFilterDataValue = payload => dispatch => {
  dispatch({
    type: types.SET_FILTER_DATA,
    payload,
  });
};
export const favouritePropertyDataGet = payload => async dispatch => {
  dispatch({ type: types.DETAIL_PROPERTY_REQUEST, payload });
  try {
    //console.log(payload, 'payload');
    const response = await favouriteDataGet(payload);
    // console.log('response', response.data);
    dispatch({
      type: types.DETAIL_PROPERTY_SUCCESS,
      payload: response.data,
    });
    return response.data;
  } catch (err) {
    dispatch({ type: types.DETAL_PROPERTY_FAILURE, payload: err });
    ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
    throw err;
  }
};
export const favouritePropertyData = payload => async dispatch => {
  dispatch({ type: types.FAVOURITE_PROPERTY_REQUEST });
  try {
    //  console.log(payload, 'payload');
    const response = await favouriteData(payload);
    //  console.log('response', response.data);
    dispatch({
      type: types.FAVOURITE_PROPERTY_SUCCESS,
      payload: response.data,
    });
    if (payload.is_favourite) {
      ToastAndroid.show('Favourite added Sucessfully!', ToastAndroid.SHORT);
    } else {
      ToastAndroid.show('Favourite removed Sucessfully!', ToastAndroid.SHORT);
    }
    return response.data;
  } catch (err) {
    console.log('error', err.response.data);
    dispatch({
      type: types.FAVOURITE_PROPERTY_FAILURE,
      payload: err.response.data,
    });
    ToastAndroid.show('Something Went Wrong!', ToastAndroid.SHORT);
    throw err;
  }
};

export const postPropertyData = payload => async dispatch => {
  dispatch({ type: types.POST_PROPERTY_REQUEST });
  try {
    console.log(payload, 'payload');
    const response = await propertyPost(payload);
    console.log('response', response);
    dispatch({
      type: types.POST_PROPERTY_SUCCESS,
      payload: response.data,
    });
    ToastAndroid.show('Property Added Sucessfully!', ToastAndroid.SHORT);
    return response.data;
  } catch (err) {
    console.log('error', err.response.data);
    dispatch({ type: types.POST_PROPERTY_FAILURE, payload: err.response.data });
    ToastAndroid.show('Something Went Wrong!', ToastAndroid.SHORT);
    throw err;
  }
};
export const mediaUpload = payload => async dispatch => {
  dispatch({ type: types.MULTIPLE_PHOTO_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await multiplePhoto(payload);
    console.log('response', response.data);
    dispatch({
      type: types.MULTIPLE_PHOTO_SUCCESS,
      payload: response.data,
    });
    return response.data;
  } catch (err) {
    dispatch({ type: types.MULTIPLE_PHOTO_FAILURE, payload: err });
    throw err;
  }
};

export const clearPostPropertyField = () => dispatch => {
  dispatch({
    type: types.CLEAR_POST_PROPERTY_FIELD,
  });
};
export const setPostPropertyValueBasic = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_BASIC,
    payload,
  });
};
export const setPostPropertyValueAddress = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_ADDRESS,
    payload,
  });
};
export const setPostPropertyValueLocationProperty = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_LOCATION_PROPERTY,
    payload,
  });
};
export const setPostPropertyValueBuilding = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_BUILDING,
    payload,
  });
};
export const setPostPropertyValueBuildingNoOf = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_NO_OF,
    payload,
  });
};
export const setPostPropertyValueMedia = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_MEDIA,
    payload,
  });
};
export const setPostPropertyValuePrice = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_PRICE,
    payload,
  });
};
export const setPostPropertyValueAgencyId = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_AGENCY_ID,
    payload,
  });
};
export const setPostPropertyValueTags = payload => dispatch => {
  dispatch({
    type: types.SET_POST_PROPERTY_VALUE_TAGS,
    payload,
  });
};
