import { createSelector } from 'reselect';

const selectProperty = state => state.property;

export const selectRecentData = createSelector(
  [selectProperty],
  property => property.recentdata,
);
export const selectDataLoading = createSelector(
  [selectProperty],
  property => property.loading,
);

export const selectHotData = createSelector(
  [selectProperty],
  property => property.hotdata,
);

export const selectTrendingData = createSelector(
  [selectProperty],
  property => property.trendingdata,
);
export const selectProjectData = createSelector(
  [selectProperty],
  property => property.projectData,
);
export const selectFilterData = createSelector(
  [selectProperty],
  property => property.filterdata,
);
export const selectDetailData = createSelector(
  [selectProperty],
  property => property.detailData,
);
export const selectFavouriteData = createSelector(
  [selectProperty],
  property => property.favourite,
);
export const setQueryData = createSelector(
  [selectProperty],
  property => property.query,
);

export const selectPropertyData = createSelector(
  [selectProperty],
  property => property.postProperty,
);

export const selectPropertyBasic = createSelector(
  [selectPropertyData],
  property => property.basic,
);

export const selectPropertyDataBasic = createSelector(
  [selectPropertyBasic],
  basic => {
    return {
      property_purpose: basic.property_purpose,
      property_category: basic.property_category,
      title: basic.title,
      description: basic.description,
      property_type: basic.property_type,
    };
  },
);

export const selectPropertyPrice = createSelector(
  [selectPropertyData],
  property => property.price,
);
export const selectPropertyDataPrice = createSelector(
  [selectPropertyPrice],
  price => {
    return {
      value: price.value,
      currency: price.currency,
      label: price.label,
    };
  },
);

export const selectPropertyAddress = createSelector(
  [selectPropertyData],
  property => property.address,
);
export const selectPropertyDataAddress = createSelector(
  [selectPropertyAddress],
  address => {
    return {
      state_id: address.state_id, // enum
      district_id: address.district_id, // enum
      city_id: address.city_id, // enum
      area_id: address.area_id, // enum
      house_no: address.house_no,
    };
  },
);
export const selectPropertyBuilding = createSelector(
  [selectPropertyData],
  property => property.building,
);

export const selectPropertyDataBuilding = createSelector(
  [selectPropertyBuilding],
  building => {
    return {
      built_year: building.built_year,
      built_month: building.built_month,
      calender_type: building.calender_type, // AD,BS
      total_floor: building.total_floor,
      furnishing: building.furnishing, // Full,Semi,Un
      kitchen: building.no_of.kitchen,
      dinningroom: building.no_of.dinningroom,
      bedroom: building.no_of.bedroom,
      bathroom: building.no_of.bathroom,
      hall: building.no_of.hall,
      // no_of: {
      //   kitchen: building.no_of.kitchen,
      //   dinningroom: building.no_of.dinningroom,
      //   bedroom: building.,
      //   bathroom: 1,
      //   hall: 1,
      // },
      parking: building.parking,
      amenities: building.amenities, // enum
    };
  },
);
export const selectLocationProperty = createSelector(
  [selectPropertyData],
  property => property.location_property,
);
export const selectLocationPropertyData = createSelector(
  [selectLocationProperty],
  location_property => {
    return {
      total_area_unit: location_property.total_area_unit, // enum
      total_area: location_property.total_area,
      built_area: location_property.built_area,
      built_area_unit: location_property.built_area_unit, // enum
      property_face: location_property.property_face, // enum
      road_access_value: location_property.road_access_value,
      road_access_length_unit: location_property.road_access_length_unit, // enum
      road_access_road_type: location_property.road_access_road_type, // enum
    };
  },
);

export const selectPropertyMedia = createSelector(
  [selectPropertyData],
  property => property.media,
);
export const selectMediaData = createSelector(
  [selectPropertyMedia],
  media => {
    return {
      images: media.images,
      youtube_video_id: media.youtube_video_id,
    };
  },
);
export const selectPropertyTags = createSelector(
  [selectPropertyData],
  property => property.tags,
);
// export const selectPropertyTagsData = createSelector(
//   [selectPropertyTags],
//   tags => {
//     return {
//       tags,
//     };
//   },
// );