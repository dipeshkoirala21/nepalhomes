export const SET_CURRENT_USER = 'mobile-app/auth/SET_CURRENT_USER';

export const NEWS_DATA_REQUEST = 'mobile-app/news/NEWS_DATA_REQUEST';
export const NEWS_DATA_SUCCESS = 'mobile-app/news/NEWS_DATA_SUCCESS';
export const NEWS_DATA_FAILURE = 'mobile-app/news/NEWS_DATA_FAILURE';

export const BLOG_DATA_REQUEST = 'mobile-app/news/BLOG_DATA_REQUEST';
export const BLOG_DATA_SUCCESS = 'mobile-app/news/BLOG_DATA_SUCCESS';
export const BLOG_DATA_FAILURE = 'mobile-app/news/BLOG_DATA_FAILURE';

export const BLOG_CAT_DATA_REQUEST = 'mobile-app/news/BLOG_CAT_DATA_REQUEST';
export const BLOG_CAT_DATA_SUCCESS = 'mobile-app/news/BLOG_CAT_DATA_SUCCESS';
export const BLOG_CAT_DATA_FAILURE = 'mobile-app/news/BLOG_CAT_DATA_FAILURE';
