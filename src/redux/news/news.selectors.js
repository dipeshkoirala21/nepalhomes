import { createSelector } from 'reselect';

const selectNews = state => state.news;

export const selectData = createSelector(
  [selectNews],
  news => news.data,
);
export const selectBlog = createSelector(
  [selectNews],
  news => news.blog,
);
export const selectLoading = createSelector(
  [selectNews],
  news => news.loading,
);
export const selectBlogCategory = createSelector(
  [selectNews],
  news => news.blogCategory,
);
