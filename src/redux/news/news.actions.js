import * as types from './news.types';
import { newsGet, blogCatGet, optionsGet } from '../../api';

export const newsData = () => async dispatch => {
  dispatch({ type: types.NEWS_DATA_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await newsGet();
    //console.log('response', response.data);
    dispatch({ type: types.NEWS_DATA_SUCCESS, payload: response.data });
    return response;
  } catch (err) {
    dispatch({ type: types.NEWS_DATA_FAILURE, payload: err });
    throw err;
  }
};
export const blogData = payload => async dispatch => {
  dispatch({ type: types.BLOG_DATA_REQUEST });
  try {
    //console.log(payload, 'payload');
    const response = await optionsGet(payload);
    //console.log('response', response.data);
    dispatch({ type: types.BLOG_DATA_SUCCESS, payload: response.data });
    return response;
  } catch (err) {
    dispatch({ type: types.BLOG_DATA_FAILURE, payload: err });
    throw err;
  }
};

export const blogCategoryData = () => async dispatch => {
  dispatch({ type: types.BLOG_CAT_DATA_REQUEST });
  try {
    // console.log(payload, 'payload');
    const response = await blogCatGet();
    //console.log('response', response.data);
    dispatch({ type: types.BLOG_CAT_DATA_SUCCESS, payload: response.data });
    return response;
  } catch (err) {
    dispatch({ type: types.BLOG_CAT_DATA_FAILURE, payload: err });
    throw err;
  }
};
