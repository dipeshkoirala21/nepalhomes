import produce from 'immer';
import * as types from './news.types';

const INITIAL_STATE = {
  loading: false,
  data: {},
  // blog: {},
  blogCategory: {},
};
const reducer = (state = INITIAL_STATE, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.NEWS_DATA_REQUEST:
        draft.loading = true;
        break;
      case types.NEWS_DATA_SUCCESS:
        draft.data = action.payload;
        draft.loading = false;
        break;
      case types.NEWS_DATA_FAILURE:
        draft.loading = false;
        break;
      case types.BLOG_DATA_SUCCESS:
        draft.data = action.payload;
        break;
      // case types.BLOG_CAT_DATA_REQUEST:
      //   draft.loading = true;
      //   break;
      case types.BLOG_CAT_DATA_SUCCESS:
        draft.blogCategory = action.payload;
        draft.loading = false;
        break;
      // case types.BLOG_CAT_DATA_FAILURE:
      //   draft.loading = false;
      //   break;
    }
  });
export default reducer;
