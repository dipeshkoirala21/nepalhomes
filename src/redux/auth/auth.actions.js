import * as types from './auth.types';
import {
  loginPost,
  registerPost,
  forgotPasswordPost,
  changePasswordPost,
  profileGet,
  profilePost,
  uploadPhoto,
  facebookLogin,
  googleLogin,
} from '../../api';

import { ToastAndroid } from 'react-native';

export const setCurrentUser = user => ({
  type: types.SET_CURRENT_USER,
  payload: user,
});

export const loginRequest = payload => async dispatch => {
  dispatch({ type: types.LOGIN_REQUEST, payload });
  try {
    // console.log(payload);
    const response = await loginPost(payload);
    //console.log(response, "response");
    dispatch({ type: types.LOGIN_SUCCESS, payload: response.data });
    ToastAndroid.show('Logged in Sucessfully!', ToastAndroid.SHORT);
  } catch (err) {
    dispatch({ type: types.LOGIN_FAILURE, payload: err });
    ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
    throw err;
  }
};
export const facebook = payload => async dispatch => {
  dispatch({ type: types.FACEBOOK_LOGIN_REQUEST, payload });
  try {
    // console.log(payload);
    const response = await facebookLogin(payload.data);
    // console.log(response.data, "response");
    dispatch({
      type: types.FACEBOOK_LOGIN_SUCCESS,
      payload: response.data,
    });
    ToastAndroid.show('Logged in Sucessfully!', ToastAndroid.SHORT);
  } catch (err) {
    dispatch({
      type: types.FACEBOOK_LOGIN_FAILURE,
      payload: err,
    });
    ToastAndroid.show(err.response.data, ToastAndroid.SHORT);
    throw err;
  }
};
export const google = payload => async dispatch => {
  dispatch({ type: types.GOOGLE_LOGIN_REQUEST, payload });
  try {
    // console.log(payload);
    const response = await googleLogin(payload.data);
    //console.log(response, "response");
    dispatch({ type: types.GOOGLE_LOGIN_SUCCESS, payload: response.data });
    ToastAndroid.show('Logged in Sucessfully!', ToastAndroid.SHORT);
  } catch (err) {
    dispatch({ type: types.GOOGLE_LOGIN_FAILURE, payload: err });
    ToastAndroid.show(err.response.data, ToastAndroid.SHORT);
    throw err;
  }
};

export const profileInfo = payload => async dispatch => {
  dispatch({ type: types.GET_PROFILE_INFO_REQUEST, payload });
  try {
    //console.log(payload, 'payload');
    const response = await profileGet();
    // console.log('response', response.data);
    dispatch({ type: types.GET_PROFILE_INFO_SUCCESS, payload: response.data });
  } catch (err) {
    dispatch({ type: types.GET_PROFILE_INFO_FAILURE, payload: err });
    ToastAndroid.show(
      'Could not get profile information..',
      ToastAndroid.SHORT,
    );
    throw err;
  }
};

export const profilePhotoUpload = payload => async dispatch => {
  dispatch({ type: types.UPLOAD_PHOTO_REQUEST, payload });
  try {
    // console.log(payload, 'payload');
    const response = await uploadPhoto(payload);
    //console.log('response from profilePhotoUpload', response);
    dispatch({ type: types.UPLOAD_PHOTO_SUCCESS, payload: response.data });
  } catch (err) {
    dispatch({ type: types.UPLOAD_PHOTO_FAILURE, payload: err });
    throw err;
  }
};

export const changeprofileInfo = payload => async dispatch => {
  dispatch({ type: types.CHANGE_PROFILE_INFO_REQUEST, payload });
  try {
    //console.log(payload, 'payload');
    const response = await profilePost(payload);
    // console.log('response', response.data);
    dispatch({
      type: types.CHANGE_PROFILE_INFO_SUCCESS,
      payload: response.data,
    });
  } catch (err) {
    dispatch({ type: types.CHANGE_PROFILE_INFO_FAILURE, payload: err });
    ToastAndroid.show('Something went wrong', ToastAndroid.SHORT);
    throw err;
  }
};

export const logout = () => ({
  type: types.LOGOUT_SUCCESS,
});

export const registerRequest = payload => async dispatch => {
  dispatch({ type: types.REGISTER_REQUEST, payload });
  try {
    const response = await registerPost(payload);
    dispatch({ type: types.REGISTER_SUCCESS, payload: response.data });
    ToastAndroid.show('Signed Up Sucessfully!', ToastAndroid.SHORT);
  } catch (err) {
    dispatch({ type: types.REGISTER_FAILURE, payload: err });
    ToastAndroid.show('Something Went Wrong!', ToastAndroid.SHORT);
    throw err;
  }
};

export const forgotPasswordRequest = payload => async dispatch => {
  dispatch({ type: types.FORGOT_PASSWORD_REQUEST, payload });
  try {
    const response = await forgotPasswordPost(payload);
    dispatch({ type: types.FORGOT_PASSWORD_SUCCESS, payload: response.data });
  } catch (err) {
    dispatch({ type: types.FORGOT_PASSWORD_FAILURE, payload: err });
    throw err;
  }
};
export const changePasswordRequest = payload => async dispatch => {
  dispatch({ type: types.CHANGE_PASSWORD_REQUEST, payload });
  try {
    const response = await changePasswordPost(payload);
    dispatch({ type: types.CHANGE_PASSWORD_SUCCESS, payload: response.data });
    ToastAndroid.show('Password Changed Sucessfully!', ToastAndroid.SHORT);
    return response;
  } catch (err) {
    dispatch({ type: types.CHANGE_PASSWORD_FAILURE, payload: err });
    ToastAndroid.show('Something Went Wrong!', ToastAndroid.SHORT);
    throw err;
  }
};
export const setProfileData = payload => dispatch => {
  dispatch({
    type: types.SET_PROFILE_VALUE,
    payload,
  });
};
