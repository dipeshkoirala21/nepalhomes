/* eslint-disable no-fallthrough */
import produce from 'immer';
import * as types from './auth.types';

const INITIAL_STATE = {
  currentUser: null,
  loading: false,
  error: {},
  data: {
    name: '',
    email: '',
    roles: [],
    avatar: null,
    date_of_birth: '',
    email_verified: false,
    image: {},
  },
};

const reducer = (state = INITIAL_STATE, action) =>
  produce(state, draft => {
    switch (action.type) {
      case types.SET_LOADING:
        draft.loading = action.payload;
        break;
      case types.LOGIN_SUCCESS:
      case types.FACEBOOK_LOGIN_SUCCESS:
      case types.GOOGLE_LOGIN_SUCCESS:
      case types.REGISTER_SUCCESS:
        draft.currentUser = action.payload.data;
        break;
      case types.SET_CURRENT_USER:
        draft.currentUser = action.payload;
        break;
      case types.LOGOUT_SUCCESS:
        draft.currentUser = INITIAL_STATE.currentUser;
        draft.data = INITIAL_STATE.data;
        break;
      case types.CHANGE_PASSWORD_SUCCESS:
        draft.currentUser = INITIAL_STATE.currentUser;
        break;
      case types.LOGIN_FAILURE:
        draft.error = action.payload.data;
        break;
      case types.GET_PROFILE_INFO_SUCCESS:
        draft.data = { ...draft.data, ...action.payload.data };
        break;
      case types.UPLOAD_PHOTO_SUCCESS:
        draft.data.image = action.payload.data.image;
        break;
      case types.CHANGE_PROFILE_INFO_SUCCESS:
        draft.profileInfo = action.payload.profileInfo;
        break;
      case types.SET_PROFILE_VALUE:
        draft.data[action.payload.key] = action.payload.value;
        break;
      case types.FORGOT_PASSWORD_SUCCESS:
        draft.currentUser = INITIAL_STATE.currentUser;
        break;
    }
  });

export default reducer;
