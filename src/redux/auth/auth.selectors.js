import { createSelector } from 'reselect';

const selectAuth = state => state.auth;

export const selectCurrentUser = createSelector(
  [selectAuth],
  auth => auth.currentUser,
);
export const selectData = createSelector(
  [selectAuth],
  auth => auth.data,
);

export const selectImage = createSelector(
  [selectAuth],
  auth => auth.images,
);

export const selectProfileInfo = createSelector(
  [selectAuth],
  auth => auth.profileInfo,
);