export const SET_CURRENT_USER = 'mobile-app/auth/SET_CURRENT_USER';

export const LOGIN_REQUEST = 'mobile-app/auth/LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'mobile-app/auth/LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'mobile-app/auth/LOGIN_FAILURE';

export const FACEBOOK_LOGIN_REQUEST = 'mobile-app/auth/FACEBOOK_LOGIN_REQUEST';
export const FACEBOOK_LOGIN_SUCCESS = 'mobile-app/auth/FACEBOOK_LOGIN_SUCCESS';
export const FACEBOOK_LOGIN_FAILURE = 'mobile-app/auth/FACEBOOK_LOGIN_FAILURE';

export const GOOGLE_LOGIN_REQUEST = 'mobile-app/auth/GOOGLE_LOGIN_REQUEST';
export const GOOGLE_LOGIN_SUCCESS = 'mobile-app/auth/GOOGLE_LOGIN_SUCCESS';
export const GOOGLE_LOGIN_FAILURE = 'mobile-app/auth/GOOGLE_LOGIN_FAILURE';

export const LOGOUT_REQUEST = 'mobile-app/auth/LOGOUT_REQUEST';
export const LOGOUT_SUCCESS = 'mobile-app/auth/LOGOUT_SUCCESS';
export const LOGOUT_FAILURE = 'mobile-app/auth/LOGOUT_FAILURE';

export const REGISTER_REQUEST = 'mobile-app/auth/REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'mobile-app/auth/REGISTER_SUCCESS';
export const REGISTER_FAILURE = 'mobile-app/auth/REGISTER_FAILURE';

export const FORGOT_PASSWORD_REQUEST =
  'mobile-app/auth/FORGOT_PASSWORD_REQUEST';
export const FORGOT_PASSWORD_SUCCESS =
  'mobile-app/auth/FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_FAILURE =
  'mobile-app/auth/FORGOT_PASSWORD_FAILURE';

export const CHANGE_PASSWORD_REQUEST =
  'mobile-app/auth/CHANGE_PASSWORD_REQUEST';
export const CHANGE_PASSWORD_SUCCESS =
  'mobile-app/auth/CHANGE_PASSWORD_SUCCESS';
export const CHANGE_PASSWORD_FAILURE =
  'mobile-app/auth/CHANGE_PASSWORD_FAILURE';

export const GET_PROFILE_INFO_REQUEST =
  'mobile-app/auth/GET_PROFILE_INFO_REQUEST';
export const GET_PROFILE_INFO_SUCCESS =
  'mobile-app/auth/GET_PROFILE_INFO_SUCCESS';
export const GET_PROFILE_INFO_FAILURE =
  'mobile-app/auth/GET_PROFILE_INFO_FAILURE';

export const CHANGE_PROFILE_INFO_REQUEST =
  'mobile-app/auth/CHANGE_PROFILE_INFO_REQUEST';
export const CHANGE_PROFILE_INFO_SUCCESS =
  'mobile-app/auth/CHANGE_PROFILE_INFO_SUCCESS';
export const CHANGE_PROFILE_INFO_FAILURE =
  'mobile-app/auth/CHANGE_PROFILE_INFO_FAILURE';

export const UPLOAD_PHOTO_REQUEST = 'mobile-app/auth/UPLOAD_PHOTO_REQUEST';
export const UPLOAD_PHOTO_SUCCESS = 'mobile-app/auth/UPLOAD_PHOTO_SUCCESS';
export const UPLOAD_PHOTO_FAILURE = 'mobile-app/auth/UPLOAD_PHOTO_FAILURE';

export const SET_PROFILE_VALUE = 'mobile-app/auth/SET_PROFILE_VALUE';
